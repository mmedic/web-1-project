﻿using System.IO;
using System.Web.Hosting;
using System.Xml;

namespace Web1Project.Models
{
    public class Write
    {
        public static void PosetilacToDataBase(Korisnik posetilac)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml"));
            xmlDoc.Load(path);

            XmlNode root = xmlDoc.SelectSingleNode("Korisnici");
            XmlElement element = xmlDoc.CreateElement("Korisnik");
            root.AppendChild(element);

            XmlElement idElement = xmlDoc.CreateElement("Id");
            idElement.InnerText = posetilac.Id.ToString();
            element.AppendChild(idElement);

            XmlElement korisnickoImeElement = xmlDoc.CreateElement("KorisnickoIme");
            korisnickoImeElement.InnerText = posetilac.KorisnickoIme;
            element.AppendChild(korisnickoImeElement);

            XmlElement lozinkaElement = xmlDoc.CreateElement("Lozinka");
            lozinkaElement.InnerText = posetilac.Lozinka;
            element.AppendChild(lozinkaElement);

            XmlElement imeElement = xmlDoc.CreateElement("Ime");
            imeElement.InnerText = posetilac.Ime;
            element.AppendChild(imeElement);

            XmlElement prezimeElement = xmlDoc.CreateElement("Prezime");
            prezimeElement.InnerText = posetilac.Prezime;
            element.AppendChild(prezimeElement);

            XmlElement polElement = xmlDoc.CreateElement("Pol");
            polElement.InnerText = posetilac.Pol.ToString();
            element.AppendChild(polElement);

            XmlElement ulogaElement = xmlDoc.CreateElement("Uloga");
            ulogaElement.InnerText = posetilac.Uloga.ToString();
            element.AppendChild(ulogaElement);

            XmlElement emailElement = xmlDoc.CreateElement("Email");
            emailElement.InnerText = posetilac.Email;
            element.AppendChild(emailElement);

            XmlElement datumElement = xmlDoc.CreateElement("DatumRodjenja");
            datumElement.InnerText = posetilac.DatumRodjenja.ToString("dd/MM/yyyy");
            element.AppendChild(datumElement);

            XmlElement treninziElement = xmlDoc.CreateElement("GrupniTreninzi");
            treninziElement.InnerText = "";
            element.AppendChild(treninziElement);

            XmlElement blokiranElement = xmlDoc.CreateElement("Blokiran");
            blokiranElement.InnerText = "false";
            element.AppendChild(blokiranElement);

            xmlDoc.Save(path);
        }

        public static void TrenerToDataBase(Korisnik trener)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml"));
            xmlDoc.Load(path);

            XmlNode root = xmlDoc.SelectSingleNode("Korisnici");
            XmlElement element = xmlDoc.CreateElement("Korisnik");
            root.AppendChild(element);

            XmlElement idElement = xmlDoc.CreateElement("Id");
            idElement.InnerText = trener.Id.ToString();
            element.AppendChild(idElement);

            XmlElement korisnickoImeElement = xmlDoc.CreateElement("KorisnickoIme");
            korisnickoImeElement.InnerText = trener.KorisnickoIme;
            element.AppendChild(korisnickoImeElement);

            XmlElement lozinkaElement = xmlDoc.CreateElement("Lozinka");
            lozinkaElement.InnerText = trener.Lozinka;
            element.AppendChild(lozinkaElement);

            XmlElement imeElement = xmlDoc.CreateElement("Ime");
            imeElement.InnerText = trener.Ime;
            element.AppendChild(imeElement);

            XmlElement prezimeElement = xmlDoc.CreateElement("Prezime");
            prezimeElement.InnerText = trener.Prezime;
            element.AppendChild(prezimeElement);

            XmlElement polElement = xmlDoc.CreateElement("Pol");
            polElement.InnerText = trener.Pol.ToString();
            element.AppendChild(polElement);

            XmlElement ulogaElement = xmlDoc.CreateElement("Uloga");
            ulogaElement.InnerText = trener.Uloga.ToString();
            element.AppendChild(ulogaElement);

            XmlElement emailElement = xmlDoc.CreateElement("Email");
            emailElement.InnerText = trener.Email;
            element.AppendChild(emailElement);

            XmlElement datumElement = xmlDoc.CreateElement("DatumRodjenja");
            datumElement.InnerText = trener.DatumRodjenja.ToString("dd/MM/yyyy");
            element.AppendChild(datumElement);

            XmlElement fitnescentarElement = xmlDoc.CreateElement("FitnesCentar");
            fitnescentarElement.InnerText = trener.FitnesCentarZaposlen.Id.ToString();
            element.AppendChild(fitnescentarElement);

            XmlElement treninziElement = xmlDoc.CreateElement("GrupniTreninzi");
            treninziElement.InnerText = "";
            element.AppendChild(treninziElement);

            XmlElement blokiranElement = xmlDoc.CreateElement("Blokiran");
            blokiranElement.InnerText = "false";
            element.AppendChild(blokiranElement);

            xmlDoc.Save(path);
        }

        public static void FitnesCentarToDataBase(FitnesCentar noviFitnesCentar)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/fitnescentri.xml"));
            xmlDoc.Load(path);

            XmlNode root = xmlDoc.SelectSingleNode("FitnesCentri");
            XmlElement element = xmlDoc.CreateElement("FitnesCentar");
            root.AppendChild(element);

            XmlElement IdElement = xmlDoc.CreateElement("Id");
            IdElement.InnerText = noviFitnesCentar.Id.ToString();
            element.AppendChild(IdElement);

            XmlElement nazivElement = xmlDoc.CreateElement("Naziv");
            nazivElement.InnerText = noviFitnesCentar.Naziv;
            element.AppendChild(nazivElement);

            XmlElement adresaElement = xmlDoc.CreateElement("Adresa");
            adresaElement.InnerText = noviFitnesCentar.Adresa;
            element.AppendChild(adresaElement);

            XmlElement godinaOtvaranjaElement = xmlDoc.CreateElement("GodinaOtvaranja");
            godinaOtvaranjaElement.InnerText = noviFitnesCentar.GodinaOtvaranja.ToString();
            element.AppendChild(godinaOtvaranjaElement);

            XmlElement vlasnikElement = xmlDoc.CreateElement("Vlasnik");
            vlasnikElement.InnerText = noviFitnesCentar.Vlasnik.Id.ToString();
            element.AppendChild(vlasnikElement);

            XmlElement mesecnaClanarinaElement = xmlDoc.CreateElement("CenaMesecneClanarine");
            mesecnaClanarinaElement.InnerText = noviFitnesCentar.CenaMesecneClanarine.ToString();
            element.AppendChild(mesecnaClanarinaElement);

            XmlElement godisnjaClanarinaElement = xmlDoc.CreateElement("CenaGodisnjeClanarine");
            godisnjaClanarinaElement.InnerText = noviFitnesCentar.CenaGodisnjeClanarine.ToString();
            element.AppendChild(godisnjaClanarinaElement);

            XmlElement jednogTreningaElement = xmlDoc.CreateElement("CenaJednogTreninga");
            jednogTreningaElement.InnerText = noviFitnesCentar.CenaJednogTreninga.ToString();
            element.AppendChild(jednogTreningaElement);

            XmlElement jednogGrupnogElement = xmlDoc.CreateElement("CenaJednogGrupnog");
            jednogGrupnogElement.InnerText = noviFitnesCentar.CenaJednogGrupnog.ToString();
            element.AppendChild(jednogGrupnogElement);

            XmlElement jednogPTElement = xmlDoc.CreateElement("CenaJednogPT");
            jednogPTElement.InnerText = noviFitnesCentar.CenaJednogPT.ToString();
            element.AppendChild(jednogPTElement);

            XmlElement obrisanElement = xmlDoc.CreateElement("Obrisan");
            obrisanElement.InnerText = noviFitnesCentar.Obrisan.ToString();
            element.AppendChild(obrisanElement);

            xmlDoc.Save(path);
        }

        public static void GrupniTreningToDataBase(GrupniTrening grupniTrening)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml"));
            xmlDoc.Load(path);

            XmlNode root = xmlDoc.SelectSingleNode("GrupniTreninzi");
            XmlElement element = xmlDoc.CreateElement("GrupniTrening");
            root.AppendChild(element);

            XmlElement IdElement = xmlDoc.CreateElement("Id");
            IdElement.InnerText = grupniTrening.Id.ToString();
            element.AppendChild(IdElement);

            XmlElement nazivElement = xmlDoc.CreateElement("Naziv");
            nazivElement.InnerText = grupniTrening.Naziv;
            element.AppendChild(nazivElement);

            XmlElement tipTreningaElement = xmlDoc.CreateElement("TipTreninga");
            tipTreningaElement.InnerText = grupniTrening.TipTreninga.ToString();
            element.AppendChild(tipTreningaElement);

            XmlElement centarElement = xmlDoc.CreateElement("FitnesCentar");
            centarElement.InnerText = grupniTrening.FitnesCentar.Id.ToString();
            element.AppendChild(centarElement);

            XmlElement trajanjeElement = xmlDoc.CreateElement("TrajanjeTreninga");
            trajanjeElement.InnerText = grupniTrening.TrajanjeTreninga.ToString();
            element.AppendChild(trajanjeElement);

            XmlElement datumElement = xmlDoc.CreateElement("VremeOdrzavanja");
            datumElement.InnerText = grupniTrening.VremeOdrzavanja.ToString("dd/MM/yyyy HH:mm");
            element.AppendChild(datumElement);

            XmlElement maxPosetilacaElement = xmlDoc.CreateElement("MaksimalanBrojPosetilaca");
            maxPosetilacaElement.InnerText = grupniTrening.MaksimalanBrojPosetilaca.ToString();
            element.AppendChild(maxPosetilacaElement);

            XmlElement posetiociElement = xmlDoc.CreateElement("Posetioci");
            posetiociElement.InnerText = "";
            element.AppendChild(posetiociElement);

            XmlElement obrisanElement = xmlDoc.CreateElement("Obrisan");
            obrisanElement.InnerText = grupniTrening.Obrisan.ToString();
            element.AppendChild(obrisanElement);

            xmlDoc.Save(path);
        }

        public static void KomentarToDataBase(Komentar komentar)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/komentari.xml"));
            xmlDoc.Load(path);

            XmlNode root = xmlDoc.SelectSingleNode("Komentari");
            XmlElement element = xmlDoc.CreateElement("Komentar");
            root.AppendChild(element);

            XmlElement IdElement = xmlDoc.CreateElement("Id");
            IdElement.InnerText = komentar.Id.ToString();
            element.AppendChild(IdElement);

            XmlElement posetilacElement = xmlDoc.CreateElement("Posetilac");
            posetilacElement.InnerText = komentar.Posetilac.Id.ToString();
            element.AppendChild(posetilacElement);

            XmlElement fitnesCentarElement = xmlDoc.CreateElement("FitnesCentar");
            fitnesCentarElement.InnerText = komentar.FitnesCentar.Id.ToString();
            element.AppendChild(fitnesCentarElement);

            XmlElement tekstElement = xmlDoc.CreateElement("Tekst");
            tekstElement.InnerText = komentar.Tekst;
            element.AppendChild(tekstElement);

            XmlElement ocenaElement = xmlDoc.CreateElement("Ocena");
            ocenaElement.InnerText = komentar.Ocena.ToString();
            element.AppendChild(ocenaElement);

            XmlElement obrisanElement = xmlDoc.CreateElement("Obrisan");
            obrisanElement.InnerText = komentar.Obrisan.ToString();
            element.AppendChild(obrisanElement);

            XmlElement odobrenElement = xmlDoc.CreateElement("Odobren");
            odobrenElement.InnerText = komentar.Odobren.ToString();
            element.AppendChild(odobrenElement);

            xmlDoc.Save(path);
        }
    }
}