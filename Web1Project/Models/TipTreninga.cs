﻿namespace Web1Project.Models
{
    public enum TipTreninga
    {
        YOGA,
        LESMILLSTONE,
        BODYPUMP,
        BODYBALANCE,
        BODYCOMBAT,
        CARDIO,
        DANCE,
        FULLBODY,
        AEROBIC
    }
}