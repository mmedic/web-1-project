﻿namespace Web1Project.Models
{
    public class FitnesCentar
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Adresa { get; set; }
        public int GodinaOtvaranja { get; set; }
        public Korisnik Vlasnik { get; set; }
        public int CenaMesecneClanarine { get; set; }
        public int CenaGodisnjeClanarine { get; set; }
        public int CenaJednogTreninga { get; set; }
        public int CenaJednogGrupnog { get; set; }
        public int CenaJednogPT { get; set; }
        public bool Obrisan { get; set; }

        public FitnesCentar()
        {

        }
        public FitnesCentar(Korisnik vlasnik)
        {
            Vlasnik = vlasnik;
            Obrisan = false;
        }

        public FitnesCentar(int id)
        {
            Id = id;
        }
    }
}