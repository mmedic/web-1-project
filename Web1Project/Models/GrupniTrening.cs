﻿using System;
using System.Collections.Generic;

namespace Web1Project.Models
{
    public class GrupniTrening
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public TipTreninga TipTreninga { get; set; }
        public FitnesCentar FitnesCentar { get; set; }
        public int TrajanjeTreninga { get; set; }
        public DateTime VremeOdrzavanja { get; set; }
        public int MaksimalanBrojPosetilaca { get; set; }
        public List<Korisnik> Posetioci { get; set; }
        public bool Obrisan { get; set; }

        public GrupniTrening()
        {
            FitnesCentar = new FitnesCentar();
        }
    }
}