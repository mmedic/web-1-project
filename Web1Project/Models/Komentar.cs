﻿namespace Web1Project.Models
{
    public class Komentar
    {
        public int Id { get; set; }
        public Korisnik Posetilac { get; set; }
        public FitnesCentar FitnesCentar { get; set; }
        public string Tekst { get; set; }
        public int Ocena { get; set; }
        public bool Obrisan { get; set; }
        public bool Odobren { get; set; }

        public Komentar()
        {
            FitnesCentar = new FitnesCentar();
            Posetilac = new Korisnik();
        }
    }
}