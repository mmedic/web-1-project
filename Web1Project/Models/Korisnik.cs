﻿using System;
using System.Collections.Generic;

namespace Web1Project.Models
{
    public class Korisnik
    {
        public int Id { get; set; }
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public Pol Pol { get; set; }
        public string Email { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public Uloga Uloga { get; set; }
        public bool LogedIn { get; set; }
        public bool Blokiran { get; set; }

        // Posetilac
        public List<GrupniTrening> PosecujeGrupneTreninge { get; set; } 

        // Trener
        public List<GrupniTrening> GrupniTreninziAngazovan { get; set; }  
        public FitnesCentar FitnesCentarZaposlen { get; set; }    

        // Vlasnik
        public List<FitnesCentar> FitnesCentriPoseduje { get; set; }  

        public Korisnik()
        {
            LogedIn = false;
        }

        public void LogIn()
        {
            LogedIn = true;
        }

        public void LogOut()
        {
            LogedIn = false;
        }
    }
}