﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using System.Xml;

namespace Web1Project.Models
{
    public class Load
    {
        // Ucitavanje Korisnika svih
        public static Dictionary<string, Korisnik> KorisnikFromDataBaseObrisani()
        {
            Dictionary<string, Korisnik> korisnici = new Dictionary<string, Korisnik>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach (XmlNode node in nodeList)
            {
                Korisnik korisnik = new Korisnik();

                korisnik.Blokiran = bool.Parse(node.SelectSingleNode("Blokiran").InnerText);
                korisnik.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                korisnik.KorisnickoIme = node.SelectSingleNode("KorisnickoIme").InnerText;
                korisnik.Lozinka = node.SelectSingleNode("Lozinka").InnerText;
                korisnik.Ime = node.SelectSingleNode("Ime").InnerText;
                korisnik.Prezime = node.SelectSingleNode("Prezime").InnerText;
                korisnik.Email = node.SelectSingleNode("Email").InnerText;
                korisnik.DatumRodjenja = Convert.ToDateTime(node.SelectSingleNode("DatumRodjenja").InnerText);

                Enum.TryParse(node.SelectSingleNode("Pol").InnerText, out Pol pol);
                korisnik.Pol = pol;

                Enum.TryParse(node.SelectSingleNode("Uloga").InnerText, out Uloga uloga);
                korisnik.Uloga = uloga;

                if (uloga == Uloga.POSETILAC)
                {
                    korisnik.PosecujeGrupneTreninge = new List<GrupniTrening>();

                    foreach (XmlNode n in node.SelectNodes("GrupniTreninzi/GrupniTrening"))
                    {
                        GrupniTrening grupni = new GrupniTrening();
                        if (n.InnerText != "")
                        {
                            grupni = GrupniTreningFromDataBaseToPosetilac(n.InnerText);
                            if(grupni != null)
                            {
                                korisnik.PosecujeGrupneTreninge.Add(grupni);
                            }
                        }
                    }
                }
                else if (uloga == Uloga.TRENER)
                {
                    korisnik.GrupniTreninziAngazovan = new List<GrupniTrening>();

                    foreach (XmlNode n in node.SelectNodes("GrupniTreninzi/GrupniTrening"))
                    {
                        if (n.InnerText != "")
                        {
                            GrupniTrening grupni = GrupniTreningFromDataBaseToTrener(n.InnerText);

                            if (grupni != null)
                            {
                                korisnik.GrupniTreninziAngazovan.Add(grupni);
                            }
                        }    
                    }
                    korisnik.FitnesCentarZaposlen = FitnesCentarFromDataBaseToKorisnik(node.SelectSingleNode("FitnesCentar").InnerText);
                }
                else if (uloga == Uloga.VLASNIK)
                {
                    korisnik.FitnesCentriPoseduje = new List<FitnesCentar>();

                    foreach (XmlNode n in node.SelectNodes("FitnesCentri/FitnesCentar"))
                    {
                        if (n.InnerText != "")
                        {
                            FitnesCentar fitnescentar = FitnesCentarFromDataBaseToKorisnik(n.InnerText);
                            if (fitnescentar != null)
                            {
                                korisnik.FitnesCentriPoseduje.Add(fitnescentar);
                            }
                        }   
                    }
                }
                korisnici.Add(korisnik.KorisnickoIme, korisnik);               
            }
            return korisnici;
        }

        // Ucitavanje Korisnika samo aktivnih
        public static Dictionary<string, Korisnik> KorisnikFromDataBase()
        {
            Dictionary<string, Korisnik> korisnici = new Dictionary<string, Korisnik>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach (XmlNode node in nodeList)
            {
                Korisnik korisnik = new Korisnik();
                korisnik.Blokiran = bool.Parse(node.SelectSingleNode("Blokiran").InnerText);

                if (!korisnik.Blokiran)
                {
                    korisnik.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                    korisnik.KorisnickoIme = node.SelectSingleNode("KorisnickoIme").InnerText;
                    korisnik.Lozinka = node.SelectSingleNode("Lozinka").InnerText;
                    korisnik.Ime = node.SelectSingleNode("Ime").InnerText;
                    korisnik.Prezime = node.SelectSingleNode("Prezime").InnerText;
                    korisnik.Email = node.SelectSingleNode("Email").InnerText;
                    korisnik.DatumRodjenja = Convert.ToDateTime(node.SelectSingleNode("DatumRodjenja").InnerText);

                    Enum.TryParse(node.SelectSingleNode("Pol").InnerText, out Pol pol);
                    korisnik.Pol = pol;

                    Enum.TryParse(node.SelectSingleNode("Uloga").InnerText, out Uloga uloga);
                    korisnik.Uloga = uloga;

                    if (uloga == Uloga.POSETILAC)
                    {
                        korisnik.PosecujeGrupneTreninge = new List<GrupniTrening>();

                        foreach (XmlNode n in node.SelectNodes("GrupniTreninzi/GrupniTrening"))
                        {
                            GrupniTrening grupni = new GrupniTrening();
                            if (n.InnerText != "")
                            {
                                grupni = GrupniTreningFromDataBaseToPosetilac(n.InnerText);
                                if (grupni != null)
                                {
                                    korisnik.PosecujeGrupneTreninge.Add(grupni);
                                }
                            }
                        }

                    }
                    else if (uloga == Uloga.TRENER)
                    {
                        korisnik.GrupniTreninziAngazovan = new List<GrupniTrening>();

                        foreach (XmlNode n in node.SelectNodes("GrupniTreninzi/GrupniTrening"))
                        {
                            if (n.InnerText != "")
                            {
                                GrupniTrening grupni = GrupniTreningFromDataBaseToTrener(n.InnerText);

                                if (grupni != null)
                                {
                                    korisnik.GrupniTreninziAngazovan.Add(grupni);
                                }
                            }
                        }

                        korisnik.FitnesCentarZaposlen = FitnesCentarFromDataBaseToKorisnik(node.SelectSingleNode("FitnesCentar").InnerText);
                    }
                    else if (uloga == Uloga.VLASNIK)
                    {
                        korisnik.FitnesCentriPoseduje = new List<FitnesCentar>();

                        foreach (XmlNode n in node.SelectNodes("FitnesCentri/FitnesCentar"))
                        {
                            if (n.InnerText != "")
                            {
                                FitnesCentar fitnescentar = FitnesCentarFromDataBaseToKorisnik(n.InnerText);
                                if (fitnescentar != null)
                                {
                                    korisnik.FitnesCentriPoseduje.Add(fitnescentar);
                                }
                            }
                        }
                    }

                    korisnici.Add(korisnik.KorisnickoIme, korisnik);
                }
            }
            return korisnici;
        }

        public static GrupniTrening GrupniTreningFromDataBaseToPosetilac(string id)
        {
            GrupniTrening grupniTrening = new GrupniTrening();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

            foreach(XmlNode node in nodeList)
            {
                if(node.SelectSingleNode("Id").InnerText == id)
                {
                    grupniTrening.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);
                    if (!grupniTrening.Obrisan)
                    {
                        grupniTrening.Id = int.Parse(id);
                        grupniTrening.Naziv = node.SelectSingleNode("Naziv").InnerText;

                        Enum.TryParse(node.SelectSingleNode("TipTreninga").InnerText, out TipTreninga tip);
                        grupniTrening.TipTreninga = tip;

                        //  Treba da zna detalje Fitnes centra

                        grupniTrening.FitnesCentar = FitnesCentarFromDataBaseToKorisnik(node.SelectSingleNode("FitnesCentar").InnerText);
                        grupniTrening.TrajanjeTreninga = int.Parse(node.SelectSingleNode("TrajanjeTreninga").InnerText);
                        grupniTrening.VremeOdrzavanja = DateTime.Parse(node.SelectSingleNode("VremeOdrzavanja").InnerText);
                        grupniTrening.MaksimalanBrojPosetilaca = int.Parse(node.SelectSingleNode("MaksimalanBrojPosetilaca").InnerText);



                        return grupniTrening;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        private static FitnesCentar FitnesCentarFromDataBaseToKorisnik(string id)
        {
            FitnesCentar fitnesCentar = new FitnesCentar();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/fitnescentri.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/FitnesCentri/FitnesCentar");

            foreach(XmlNode node in nodeList)
            {
                if(node.SelectSingleNode("Id").InnerText == id)
                {
                    fitnesCentar.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);
                    if (!fitnesCentar.Obrisan)
                    {
                        fitnesCentar.Id = int.Parse(id);
                        fitnesCentar.Naziv = node.SelectSingleNode("Naziv").InnerText;
                        fitnesCentar.Adresa = node.SelectSingleNode("Adresa").InnerText;
                        fitnesCentar.GodinaOtvaranja = int.Parse(node.SelectSingleNode("GodinaOtvaranja").InnerText);
                        fitnesCentar.CenaMesecneClanarine = int.Parse(node.SelectSingleNode("CenaMesecneClanarine").InnerText);
                        fitnesCentar.CenaGodisnjeClanarine = int.Parse(node.SelectSingleNode("CenaGodisnjeClanarine").InnerText);
                        fitnesCentar.CenaJednogTreninga = int.Parse(node.SelectSingleNode("CenaJednogTreninga").InnerText);
                        fitnesCentar.CenaJednogGrupnog = int.Parse(node.SelectSingleNode("CenaJednogGrupnog").InnerText);
                        fitnesCentar.CenaJednogPT = int.Parse(node.SelectSingleNode("CenaJednogPT").InnerText);

                        return fitnesCentar;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        public static GrupniTrening GrupniTreningFromDataBaseToTrener(string id)
        {
            GrupniTrening grupniTrening = new GrupniTrening();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

            foreach (XmlNode node in nodeList)
            {
                if (node.SelectSingleNode("Id").InnerText == id)
                {
                    grupniTrening.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);
                    if (!grupniTrening.Obrisan)
                    {
                        grupniTrening.Id = int.Parse(id);
                        grupniTrening.Naziv = node.SelectSingleNode("Naziv").InnerText;

                        Enum.TryParse(node.SelectSingleNode("TipTreninga").InnerText, out TipTreninga tip);
                        grupniTrening.TipTreninga = tip;

                        //  Treba dodati da zna koji je Fitnes centar upitanju

                        grupniTrening.FitnesCentar.Id = int.Parse(node.SelectSingleNode("FitnesCentar").InnerText);
                        grupniTrening.TrajanjeTreninga = int.Parse(node.SelectSingleNode("TrajanjeTreninga").InnerText);
                        grupniTrening.VremeOdrzavanja = DateTime.Parse(node.SelectSingleNode("VremeOdrzavanja").InnerText);
                        grupniTrening.MaksimalanBrojPosetilaca = int.Parse(node.SelectSingleNode("MaksimalanBrojPosetilaca").InnerText);

                        grupniTrening.Posetioci = new List<Korisnik>();
                        foreach (XmlNode n in node.SelectNodes("Posetioci/Posetilac"))
                        {
                            if(n.InnerText != "")
                            {
                                grupniTrening.Posetioci.Add(PosetilacFromDataBaseToGrupniTrening(n.InnerText));
                            }
                        }
                        return grupniTrening;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        private static Korisnik PosetilacFromDataBaseToGrupniTrening(string id)
        {
            Korisnik posetilac = new Korisnik();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach(XmlNode node in nodeList)
            {
                if(node.SelectSingleNode("Id").InnerText == id)
                {
                    posetilac.Blokiran = bool.Parse(node.SelectSingleNode("Blokiran").InnerText);
                    if (!posetilac.Blokiran)
                    {
                        posetilac.Id = int.Parse(id);
                        posetilac.KorisnickoIme = node.SelectSingleNode("KorisnickoIme").InnerText;
                        posetilac.Ime = node.SelectSingleNode("Ime").InnerText;
                        posetilac.Prezime = node.SelectSingleNode("Prezime").InnerText;

                        Enum.TryParse(node.SelectSingleNode("Pol").InnerText, out Pol pol);
                        posetilac.Pol = pol;

                        Enum.TryParse(node.SelectSingleNode("Uloga").InnerText, out Uloga uloga);
                        posetilac.Uloga = uloga;

                        posetilac.Email = node.SelectSingleNode("Email").InnerText;
                        posetilac.DatumRodjenja = Convert.ToDateTime(node.SelectSingleNode("DatumRodjenja").InnerText);

                        return posetilac;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        // Ucitavanje Fitnes Centara svih
        public static Dictionary<int, FitnesCentar> FitnesCentriFromDataBaseObrisani()
        {
            Dictionary<int, FitnesCentar> fitnescentri = new Dictionary<int, FitnesCentar>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/fitnescentri.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/FitnesCentri/FitnesCentar");

            foreach (XmlNode node in nodeList)
            {
                Korisnik vlasnik = PosetilacFromDataBaseToGrupniTrening(node.SelectSingleNode("Vlasnik").InnerText);
                
                FitnesCentar fitnescentar = new FitnesCentar(vlasnik);

                fitnescentar.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                fitnescentar.Naziv = node.SelectSingleNode("Naziv").InnerText;
                fitnescentar.Adresa = node.SelectSingleNode("Adresa").InnerText;
                fitnescentar.GodinaOtvaranja = int.Parse(node.SelectSingleNode("GodinaOtvaranja").InnerText);
                fitnescentar.CenaMesecneClanarine = int.Parse(node.SelectSingleNode("CenaMesecneClanarine").InnerText);
                fitnescentar.CenaGodisnjeClanarine = int.Parse(node.SelectSingleNode("CenaGodisnjeClanarine").InnerText);
                fitnescentar.CenaJednogTreninga = int.Parse(node.SelectSingleNode("CenaJednogTreninga").InnerText);
                fitnescentar.CenaJednogGrupnog = int.Parse(node.SelectSingleNode("CenaJednogGrupnog").InnerText);
                fitnescentar.CenaJednogPT = int.Parse(node.SelectSingleNode("CenaJednogPT").InnerText);
                fitnescentar.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);


                
                fitnescentri.Add(fitnescentar.Id, fitnescentar);
                

            }
            return fitnescentri;
        }

        // Ucitavanje Fitnes Centara samo aktivnih
        public static Dictionary<int, FitnesCentar> FitnesCentriFromDataBase()
        {
            Dictionary<int, FitnesCentar> fitnescentri = new Dictionary<int, FitnesCentar>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/fitnescentri.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/FitnesCentri/FitnesCentar");

            foreach (XmlNode node in nodeList)
            {
                Korisnik vlasnik = PosetilacFromDataBaseToGrupniTrening(node.SelectSingleNode("Vlasnik").InnerText);

                FitnesCentar fitnescentar = new FitnesCentar(vlasnik);
                fitnescentar.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);

                if (!fitnescentar.Obrisan)
                {
                    fitnescentar.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                    fitnescentar.Naziv = node.SelectSingleNode("Naziv").InnerText;
                    fitnescentar.Adresa = node.SelectSingleNode("Adresa").InnerText;
                    fitnescentar.GodinaOtvaranja = int.Parse(node.SelectSingleNode("GodinaOtvaranja").InnerText);
                    fitnescentar.CenaMesecneClanarine = int.Parse(node.SelectSingleNode("CenaMesecneClanarine").InnerText);
                    fitnescentar.CenaGodisnjeClanarine = int.Parse(node.SelectSingleNode("CenaGodisnjeClanarine").InnerText);
                    fitnescentar.CenaJednogTreninga = int.Parse(node.SelectSingleNode("CenaJednogTreninga").InnerText);
                    fitnescentar.CenaJednogGrupnog = int.Parse(node.SelectSingleNode("CenaJednogGrupnog").InnerText);
                    fitnescentar.CenaJednogPT = int.Parse(node.SelectSingleNode("CenaJednogPT").InnerText);

                    fitnescentri.Add(fitnescentar.Id, fitnescentar);
                }
            }
            return fitnescentri;
        }

        // Ucitavanje grupnih treninga svih
        public static Dictionary<int, GrupniTrening> GrupniTreninziFromDataBaseObrisani()
        {
            Dictionary<int, GrupniTrening> grupnitreninzi = new Dictionary<int, GrupniTrening>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

            foreach (XmlNode node in nodeList)
            {
                GrupniTrening grupnitrening = new GrupniTrening();
                grupnitrening.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);

                
                    grupnitrening.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                    grupnitrening.Naziv = node.SelectSingleNode("Naziv").InnerText;

                    Enum.TryParse(node.SelectSingleNode("TipTreninga").InnerText, out TipTreninga tip);
                    grupnitrening.TipTreninga = tip;
          
                    grupnitrening.FitnesCentar = FitnesCentarFromDataBaseToKorisnik(node.SelectSingleNode("FitnesCentar").InnerText);

                    grupnitrening.TrajanjeTreninga = int.Parse(node.SelectSingleNode("TrajanjeTreninga").InnerText);
                    grupnitrening.VremeOdrzavanja = Convert.ToDateTime(node.SelectSingleNode("VremeOdrzavanja").InnerText);
                    grupnitrening.MaksimalanBrojPosetilaca = int.Parse(node.SelectSingleNode("MaksimalanBrojPosetilaca").InnerText);

                    grupnitrening.Posetioci = new List<Korisnik>();         

                    foreach (XmlNode n in node.SelectNodes("Posetioci/Posetilac"))
                    {
                        Korisnik posetilac = new Korisnik();
                        if (n.InnerText != "")
                        {
                            posetilac = PosetilacFromDataBaseToGrupniTrening(n.InnerText);
                            grupnitrening.Posetioci.Add(posetilac);
                        } 
                    }

                    grupnitreninzi.Add(grupnitrening.Id, grupnitrening);
                  
            }

            return grupnitreninzi;
        }

        // Ucitavanje Grupnih treninga samo aktivnih
        public static Dictionary<int, GrupniTrening> GrupniTreninziFromDataBase()
        {
            Dictionary<int, GrupniTrening> grupnitreninzi = new Dictionary<int, GrupniTrening>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

            foreach (XmlNode node in nodeList)
            {
                GrupniTrening grupnitrening = new GrupniTrening();
                grupnitrening.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);

                if (!grupnitrening.Obrisan)
                {
                    grupnitrening.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                    grupnitrening.Naziv = node.SelectSingleNode("Naziv").InnerText;

                    Enum.TryParse(node.SelectSingleNode("TipTreninga").InnerText, out TipTreninga tip);
                    grupnitrening.TipTreninga = tip;

                    grupnitrening.FitnesCentar = FitnesCentarFromDataBaseToKorisnik(node.SelectSingleNode("FitnesCentar").InnerText);

                    grupnitrening.TrajanjeTreninga = int.Parse(node.SelectSingleNode("TrajanjeTreninga").InnerText);
                    grupnitrening.VremeOdrzavanja = Convert.ToDateTime(node.SelectSingleNode("VremeOdrzavanja").InnerText);
                    grupnitrening.MaksimalanBrojPosetilaca = int.Parse(node.SelectSingleNode("MaksimalanBrojPosetilaca").InnerText);

                    grupnitrening.Posetioci = new List<Korisnik>();

                    foreach (XmlNode n in node.SelectNodes("Posetioci/Posetilac"))
                    {
                        Korisnik posetilac = new Korisnik();
                        if (n.InnerText != "")
                        {
                            posetilac = PosetilacFromDataBaseToGrupniTrening(n.InnerText);
                            grupnitrening.Posetioci.Add(posetilac);
                        }
                    }

                    grupnitreninzi.Add(grupnitrening.Id, grupnitrening);
                }
            }
            return grupnitreninzi;
        }

        // Ucitavanje Komentara svih
        public static List<Komentar> KomenteriFromDataBaseObrisani()
        {
            List<Komentar> komentari = new List<Komentar>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/komentari.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Komentari/Komentar");

            foreach (XmlNode node in nodeList)
            {
                Komentar komentar = new Komentar();
                komentar.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);
                komentar.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                komentar.Tekst = node.SelectSingleNode("Tekst").InnerText;
                komentar.Ocena = int.Parse(node.SelectSingleNode("Ocena").InnerText);
                komentar.Posetilac = PosetilacFromDataBaseToKomentar(node.SelectSingleNode("Posetilac").InnerText);
                komentar.FitnesCentar = FitnesCentarFromDataBaseToKorisnik(node.SelectSingleNode("FitnesCentar").InnerText);
                komentar.Odobren = bool.Parse(node.SelectSingleNode("Odobren").InnerText);

                komentari.Add(komentar);
                
            }
            return komentari;
        }

        // Ucitavanje Komentara samo aktivnih
        public static List<Komentar> KomenteriFromDataBase()
        {
            List<Komentar> komentari = new List<Komentar>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/komentari.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Komentari/Komentar");

            foreach (XmlNode node in nodeList)
            {
                Komentar komentar = new Komentar();
                komentar.Obrisan = bool.Parse(node.SelectSingleNode("Obrisan").InnerText);

                if (!komentar.Obrisan)
                {
                    komentar.Id = int.Parse(node.SelectSingleNode("Id").InnerText);
                    komentar.Tekst = node.SelectSingleNode("Tekst").InnerText;
                    komentar.Ocena = int.Parse(node.SelectSingleNode("Ocena").InnerText);
                    komentar.Posetilac = PosetilacFromDataBaseToKomentar(node.SelectSingleNode("Posetilac").InnerText);
                    komentar.FitnesCentar = FitnesCentarFromDataBaseToKorisnik(node.SelectSingleNode("FitnesCentar").InnerText);
                    komentar.Odobren = bool.Parse(node.SelectSingleNode("Odobren").InnerText);

                    komentari.Add(komentar);
                }
            }
            return komentari;
        }

        private static Korisnik PosetilacFromDataBaseToKomentar(string id)
        {
            Korisnik posetilac = new Korisnik();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach (XmlNode node in nodeList)
            {
                if (node.SelectSingleNode("Id").InnerText == id)
                {
                    posetilac.Id = int.Parse(id);
                    posetilac.KorisnickoIme = node.SelectSingleNode("KorisnickoIme").InnerText;
                    posetilac.Ime = node.SelectSingleNode("Ime").InnerText;
                    posetilac.Prezime = node.SelectSingleNode("Prezime").InnerText;
                    posetilac.Email = node.SelectSingleNode("Email").InnerText;

                }
            }


            return posetilac;
        }

        public static bool KorisnikBlokiran(int id)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml")));
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach(XmlNode node in nodeList)
            {
                if(int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                {
                    return bool.Parse(node.SelectSingleNode("Blokiran").InnerText);
                }
            }
            return false;
        }

    }
}