﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using Web1Project.Models;

namespace Web1Project.Controllers
{
    public class TrenerUlogovanController : Controller
    {
        // GET: TrenerUlogovan
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult MojiGrupniTreninzi()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            
            List<GrupniTrening> sortiranitreninzistari = new List<GrupniTrening>();
            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();

            foreach(var item in trener.GrupniTreninziAngazovan.ToList())
            {
                GrupniTrening grupni = Load.GrupniTreningFromDataBaseToTrener(item.Id.ToString());
                if(grupni != null)
                {
                    if (item.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupni);
                    }
                    else
                    {
                        sortiranitreninzistari.Add(grupni);
                    }
                }
                else
                {
                    trener.GrupniTreninziAngazovan.Remove(item);
                }
            }


            sortiranitreninzistari = sortiranitreninzistari.OrderBy(l => l.VremeOdrzavanja).ToList();
            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();

            Session["pretrazenitreninzi"] = sortiranitreninzistari;
            

            Session["nazivGT"] = 0;
            Session["tip"] = 0;
            Session["vreme"] = 1;

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));

            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.korisnik = trener;
            ViewBag.trener = trener;

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }

            return View();
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Naziv()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in trener.GrupniTreninziAngazovan.ToList())
            {
                GrupniTrening grupni = Load.GrupniTreningFromDataBaseToTrener(item.Id.ToString());
                if(grupni == null)
                {
                    trener.GrupniTreninziAngazovan.Remove(item);
                }
                else
                {
                    if (item.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupni);
                    }
                }
            }

            List<GrupniTrening> grupnitreninzipretrazeni = (List<GrupniTrening>)Session["pretrazenitreninzi"];
            List<GrupniTrening> sortiranitreninzistari;
            Session["tip"] = 0;
            Session["vreme"] = 0;

            if ((int)Session["nazivGT"] == 0)
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderBy(l => l.Naziv).ToList();
                Session["nazivGT"] = 1;
            }
            else
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderByDescending(l => l.Naziv).ToList();
                Session["nazivGT"] = 0;
            }

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));

            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.korisnik = trener;
            ViewBag.trener = trener;
            return View("MojiGrupniTreninzi");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Tip()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in trener.GrupniTreninziAngazovan.ToList())
            {
                GrupniTrening grupni = Load.GrupniTreningFromDataBaseToTrener(item.Id.ToString());
                if (grupni == null)
                {
                    trener.GrupniTreninziAngazovan.Remove(item);
                }
                else
                {
                    if (item.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupni);
                    }
                }
            }


            List<GrupniTrening> grupnitreninzipretrazeni = (List<GrupniTrening>)Session["pretrazenitreninzi"];
            List<GrupniTrening> sortiranitreninzistari;
            Session["nazivGT"] = 0;
            Session["vreme"] = 0;

            if ((int)Session["tip"] == 0)
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderBy(l => l.TipTreninga).ToList();
                Session["tip"] = 1;
            }
            else
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderByDescending(l => l.TipTreninga).ToList();
                Session["tip"] = 0;
            }

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));

            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.korisnik = trener;
            ViewBag.trener = trener;
            return View("MojiGrupniTreninzi");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Vreme()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in trener.GrupniTreninziAngazovan.ToList())
            {
                GrupniTrening grupni = Load.GrupniTreningFromDataBaseToTrener(item.Id.ToString());
                if (grupni == null)
                {
                    trener.GrupniTreninziAngazovan.Remove(item);
                }
                else
                {
                    if (item.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupni);
                    }
                }
            }

            List<GrupniTrening> grupnitreninzipretrazeni = (List<GrupniTrening>)Session["pretrazenitreninzi"];
            List<GrupniTrening> sortiranitreninzistari;
            Session["nazivGT"] = 0;
            Session["tip"] = 0;

            if ((int)Session["vreme"] == 0)
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderBy(l => l.VremeOdrzavanja).ToList();
                Session["vreme"] = 1;
            }
            else
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderByDescending(l => l.VremeOdrzavanja).ToList();
                Session["vreme"] = 0;
            }

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));

            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.korisnik = trener;
            ViewBag.trener = trener;
            return View("MojiGrupniTreninzi");
        }
 
        [HttpPost]
        public ActionResult Izmeni(int id)
        {
            Session["id"] = id;

            return RedirectToAction("IzmeniGrupniTrening");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult IzmeniGrupniTrening()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)Session["id"];

            Dictionary<int, GrupniTrening> grupnitreninzi = Load.GrupniTreninziFromDataBase();
            if (!grupnitreninzi.ContainsKey(id))
            {
                return RedirectToAction("MojiGrupniTreninzi");
            }

            ViewBag.grupnitrening = grupnitreninzi[id];
            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.korisnik = trener;

            return View();
        }

        [HttpPost]
        public ActionResult GrupniTreningIzmenjen(int id, GrupniTrening noviGrupniTrening)
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }

            var tip = Request["TipTreninga"] != null ? Request["TipTreninga"] : string.Empty;

            bool flag = false;

            if (tip != "" || noviGrupniTrening.Naziv != null || noviGrupniTrening.TrajanjeTreninga != 0 || noviGrupniTrening.VremeOdrzavanja.AddMinutes(10) > DateTime.Now.AddDays(3) || noviGrupniTrening.MaksimalanBrojPosetilaca != 0)
            {
                Dictionary<int, GrupniTrening> grupnitreninzi = Load.GrupniTreninziFromDataBase();

                XmlDocument xmlDoc = new XmlDocument();
                string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml"));
                xmlDoc.Load(path);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

                if (!grupnitreninzi.ContainsKey(id))
                {
                    Session["AlertMessage"] = "Ovaj grupni trening ne postoji";
                    return RedirectToAction("MojiGrupniTreninzi");
                }
                GrupniTrening grupnitrening = grupnitreninzi[id];

                if(noviGrupniTrening.Naziv != null && noviGrupniTrening.Naziv != grupnitrening.Naziv)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            node.SelectSingleNode("Naziv").InnerText = noviGrupniTrening.Naziv;

                            foreach(var item in trener.GrupniTreninziAngazovan)
                            {
                                if(item.Id == id)
                                {
                                    item.Naziv = noviGrupniTrening.Naziv;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                if (noviGrupniTrening.TrajanjeTreninga != 0 && noviGrupniTrening.TrajanjeTreninga != grupnitrening.TrajanjeTreninga)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            node.SelectSingleNode("TrajanjeTreninga").InnerText = noviGrupniTrening.TrajanjeTreninga.ToString();

                            foreach (var item in trener.GrupniTreninziAngazovan)
                            {
                                if (item.Id == id)
                                {
                                    item.TrajanjeTreninga = noviGrupniTrening.TrajanjeTreninga;
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }
                if (noviGrupniTrening.VremeOdrzavanja.AddMinutes(10) > DateTime.Now.AddDays(3) && noviGrupniTrening.VremeOdrzavanja != grupnitrening.VremeOdrzavanja)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            node.SelectSingleNode("VremeOdrzavanja").InnerText = noviGrupniTrening.VremeOdrzavanja.ToString("dd/MM/yyyy HH:mm");

                            foreach (var item in trener.GrupniTreninziAngazovan)
                            {
                                if (item.Id == id)
                                {
                                    item.VremeOdrzavanja = noviGrupniTrening.VremeOdrzavanja;
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }
                if (noviGrupniTrening.MaksimalanBrojPosetilaca != 0 && noviGrupniTrening.MaksimalanBrojPosetilaca != grupnitrening.MaksimalanBrojPosetilaca && noviGrupniTrening.MaksimalanBrojPosetilaca >= grupnitrening.Posetioci.Count)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            node.SelectSingleNode("MaksimalanBrojPosetilaca").InnerText = noviGrupniTrening.MaksimalanBrojPosetilaca.ToString();

                            foreach (var item in trener.GrupniTreninziAngazovan)
                            {
                                if (item.Id == id)
                                {
                                    item.MaksimalanBrojPosetilaca = noviGrupniTrening.MaksimalanBrojPosetilaca;
                                    break;
                                }
                            }

                            break;
                        }
                    }
                }
                if (tip != "" && tip != grupnitrening.TipTreninga.ToString())
                {
                    if (Enum.IsDefined(typeof(TipTreninga), tip)){
                        foreach (XmlNode node in nodeList)
                        {
                            if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                            {
                                flag = true;
                                node.SelectSingleNode("TipTreninga").InnerText = tip;

                                foreach (var item in trener.GrupniTreninziAngazovan)
                                {
                                    if (item.Id == id)
                                    {
                                        Enum.TryParse(tip, out TipTreninga t);
                                        item.TipTreninga = t;
                                        break;
                                    }
                                }

                                break;
                            }
                        }
                    }
                }

                if (flag)
                {
                    xmlDoc.Save(path);

                    Session["AlertMessage"] = "Grupni trening uspesno izmenjen!";
                }
                else
                {
                    Session["AlertMessage"] = "Grupni trening nije izmenjen!";
                }
            }
            
            return RedirectToAction("MojiGrupniTreninzi");
        }

        [HttpPost]
        public ActionResult ObrisiGrupniTrening(int id)
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }

            Dictionary<int, GrupniTrening> grupnitreninzi = Load.GrupniTreninziFromDataBase();

            if(!grupnitreninzi.ContainsKey(id))
            {
                return RedirectToAction("MojiGrupniTreninzi");
            }

            if(grupnitreninzi[id].Posetioci.Count == 0)
            {
                XmlDocument xmlDoc = new XmlDocument();
                string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml"));
                xmlDoc.Load(path);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

                foreach (XmlNode node in nodeList)
                {
                    if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                    {
                        node.SelectSingleNode("Obrisan").InnerText = "true";
                        break;
                    }
                }

                xmlDoc.Save(path);

                Session["AlertMessage"] = "Grupni trening uspesno obrisan";
                return RedirectToAction("MojiGrupniTreninzi");
            }

            Session["AlertMessage"] = "Ovaj grupni trening ne moze biti obrisan";
            return RedirectToAction("MojiGrupniTreninzi");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult DodajGrupniTrening()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.korisnik = trener;
            ViewBag.trener = trener;
            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }

            return View();
        }

        [HttpPost]
        public ActionResult GrupniTreningDodat(GrupniTrening grupnitrening)
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }

            if (grupnitrening.Naziv != null && grupnitrening.Naziv != "" && grupnitrening.TrajanjeTreninga != 0 && grupnitrening.VremeOdrzavanja.AddMinutes(10) > DateTime.Now.AddDays(3) && grupnitrening.MaksimalanBrojPosetilaca != 0)
            {
                Dictionary<int, GrupniTrening> grupnitreninzi = Load.GrupniTreninziFromDataBaseObrisani();

                grupnitrening.Id = grupnitreninzi.Count;
                grupnitrening.FitnesCentar = trener.FitnesCentarZaposlen;
                grupnitrening.Posetioci = new List<Korisnik>();

                trener.GrupniTreninziAngazovan.Add(grupnitrening);

                Write.GrupniTreningToDataBase(grupnitrening);

                // Treneru upisujemo ovaj GrupniTrening
                XmlDocument xmlDoc = new XmlDocument();
                string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml"));
                xmlDoc.Load(path);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

                foreach (XmlNode node in nodeList)
                {
                    if (node.SelectSingleNode("Id").InnerText == trener.Id.ToString())
                    {
                        XmlNode root = node.SelectSingleNode("GrupniTreninzi");
                        XmlElement element = xmlDoc.CreateElement("GrupniTrening");
                        element.InnerText = grupnitrening.Id.ToString();
                        root.AppendChild(element);
                    }
                }
                xmlDoc.Save(path);

                Session["AlertMessage"] = "Grupni trening uspesno dodat!";
                return RedirectToAction("MojiGrupniTreninzi");
            }

            Session["AlertMessage"] = "Sva polja moraju biti pravilno popunjena";
            return RedirectToAction("DodajGrupniTrening");
        }

        public ActionResult Prikazi(int id)
        {
            Session["id"] = id;

            return RedirectToAction("PrikaziPosetioce");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrikaziPosetioce()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)Session["id"];

            Dictionary<int, GrupniTrening> grupnitreninzi = Load.GrupniTreninziFromDataBase();
            if (!grupnitreninzi.ContainsKey(id))
            {
                return RedirectToAction("MojiGrupniTreninzi");
            }

            ViewBag.trening = grupnitreninzi[id];
            ViewBag.korisnik = trener;

            return View();
        }

        [HttpPost]
        public ActionResult Pretrazi(string naziv, string tip)
        {
            Session["TrenerNaziv"] = naziv;
            Session["TrenerTip"] = tip;

            string vremeMinS = Request["vremeMin"] != null ? Request["vremeMin"] : string.Empty;
            string vremeMaxS = Request["vremeMax"] != null ? Request["vremeMax"] : string.Empty;

            Session["TrenerMin"] = vremeMinS;
            Session["TrenerMax"] = vremeMaxS;

            return RedirectToAction("Pretraga");
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Pretraga()
        {
            Korisnik trener = (Korisnik)Session["PrijavljeniKorisnik"];
            if (trener == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (trener.Uloga != Uloga.TRENER)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Load.KorisnikBlokiran(trener.Id))
            {
                Session["AlertMessage"] = "Blokirani ste iz sistema!";
                return RedirectToAction("Logout", "Authentication");
            }
            if (!trener.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            string naziv = (string)Session["TrenerNaziv"];
            string tip = (string)Session["TrenerTip"];

            string vremeMinS = (string)Session["TrenerMin"];
            DateTime.TryParse(vremeMinS, out DateTime vremeMin);

            string vremeMaxS = (string)Session["TrenerMax"];
            DateTime.TryParse(vremeMaxS, out DateTime vremeMax);

            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            List<GrupniTrening> sortiranitreninzipretrazeni = new List<GrupniTrening>();
            List<GrupniTrening> sortiranitreninzistari = new List<GrupniTrening>();
            foreach (var item in trener.GrupniTreninziAngazovan.ToList())
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToTrener(item.Id.ToString());
                if(grupnitrening != null)
                {
                    if (item.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupnitrening);
                    }
                    else
                    {
                        sortiranitreninzistari.Add(grupnitrening);
                    }
                }
                else
                {
                    trener.GrupniTreninziAngazovan.Remove(item);
                }
            }

            foreach(var i in sortiranitreninzistari)
            {
                if(vremeMaxS == "")
                {
                    if (i.Naziv.ToLower().Contains(naziv.ToLower()) && i.TipTreninga.ToString().Contains(tip) && i.VremeOdrzavanja > vremeMin)
                    {
                        sortiranitreninzipretrazeni.Add(i);
                    }
                }
                else
                {
                    if (i.Naziv.ToLower().Contains(naziv.ToLower()) && i.TipTreninga.ToString().Contains(tip) && i.VremeOdrzavanja > vremeMin && i.VremeOdrzavanja < vremeMax)
                    {
                        sortiranitreninzipretrazeni.Add(i);
                    }
                }
            }

            
            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.trener = trener;
            ViewBag.korisnik = trener;

            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            
            Session["pretrazenitreninzi"] = sortiranitreninzipretrazeni;
            ViewBag.sortiranitreninzistari = sortiranitreninzipretrazeni;
            return View("MojiGrupniTreninzi");
        }
    }
}