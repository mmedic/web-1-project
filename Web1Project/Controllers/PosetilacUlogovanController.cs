﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using Web1Project.Models;

namespace Web1Project.Controllers
{
    public class PosetilacUlogovanController : Controller
    {
        // GET: PosetilacUlogovan
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult MojiGrupniTreninzi()
        {
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];
            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!posetilac.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            List<GrupniTrening> sortiranitreninzistari = new List<GrupniTrening>();
            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();

            foreach (var item in posetilac.PosecujeGrupneTreninge.ToList())
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToPosetilac(item.Id.ToString());
                if(grupnitrening != null)
                {
                    if (grupnitrening.VremeOdrzavanja < DateTime.Now)
                    {
                        sortiranitreninzistari.Add(grupnitrening);
                    }
                    else
                    {
                        sortiranitreninzinovi.Add(grupnitrening);
                    }
                }
                else
                {
                    posetilac.PosecujeGrupneTreninge.Remove(item);
                }
            }

            sortiranitreninzistari = sortiranitreninzistari.OrderBy(l => l.VremeOdrzavanja).ToList();
            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();

            Session["pretrazenitreninzi"] = sortiranitreninzistari;
            Session["nazivGT"] = 0;
            Session["tip"] = 0;
            Session["vreme"] = 1;

            
            ViewBag.korisnik = posetilac;

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.posetilac = posetilac;

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }

            return View();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Naziv()
        {
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];
            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!posetilac.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in posetilac.PosecujeGrupneTreninge.ToList())
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToPosetilac(item.Id.ToString());
                if (grupnitrening != null)
                {
                    if (grupnitrening.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupnitrening);
                    }
                }
                else
                {
                    posetilac.PosecujeGrupneTreninge.Remove(item);
                }
            }

            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();

            List<GrupniTrening> grupnitreninzipretrazeni = (List<GrupniTrening>)Session["pretrazenitreninzi"];
            List<GrupniTrening> sortiranitreninzistari;
            Session["tip"] = 0;
            Session["vreme"] = 0;

            ViewBag.korisnik = posetilac;

            if ((int)Session["nazivGT"] == 0)
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderBy(l => l.Naziv).ToList();
                Session["nazivGT"] = 1;
            }
            else
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderByDescending(l => l.Naziv).ToList();
                Session["nazivGT"] = 0;
            }

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.posetilac = posetilac;
            return View("MojiGrupniTreninzi");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Tip()
        {
            //  Proveravamo da li je korisnik odjavljen (dosli smo ovde posle Back)
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];
            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!posetilac.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            //  Proveravamo da li je trening obrisan u medjuvremenu od strane drugog trenera
            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in posetilac.PosecujeGrupneTreninge.ToList())
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToPosetilac(item.Id.ToString());
                if (grupnitrening != null)
                {
                    if (grupnitrening.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupnitrening);
                    }
                }
                else
                {
                    posetilac.PosecujeGrupneTreninge.Remove(item);
                }
            }

            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();

            //  Uzimamo pretrazene stare treninge i njih sortiramo
            List<GrupniTrening> grupnitreninzipretrazeni = (List<GrupniTrening>)Session["pretrazenitreninzi"];
            List<GrupniTrening> sortiranitreninzistari;
            Session["nazivGT"] = 0;
            Session["vreme"] = 0;

            if ((int)Session["tip"] == 0)
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderBy(l => l.TipTreninga).ToList();
                Session["tip"] = 1;
            }
            else
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderByDescending(l => l.TipTreninga).ToList();
                Session["tip"] = 0;
            }

            ViewBag.korisnik = posetilac;

            //  Dodajemo sta nam treba na frontu
            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.posetilac = posetilac;
            return View("MojiGrupniTreninzi");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Vreme()
        {
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];
            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!posetilac.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            //  Proveravamo da li je trening obrisan u medjuvremenu od strane drugog trenera
            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in posetilac.PosecujeGrupneTreninge.ToList())
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToPosetilac(item.Id.ToString());
                if (grupnitrening != null)
                {
                    if (grupnitrening.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupnitrening);
                    }
                }
                else
                {
                    posetilac.PosecujeGrupneTreninge.Remove(item);
                }
            }

            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();

            //  Uzimamo pretrazene stare treninge i njih sortiramo
            List<GrupniTrening> grupnitreninzipretrazeni = (List<GrupniTrening>)Session["pretrazenitreninzi"];
            List<GrupniTrening> sortiranitreninzistari;
            Session["nazivGT"] = 0;
            Session["tip"] = 0;

            if ((int)Session["vreme"] == 0)
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderBy(l => l.VremeOdrzavanja).ToList();
                Session["vreme"] = 1;
            }
            else
            {
                sortiranitreninzistari = grupnitreninzipretrazeni.OrderByDescending(l => l.VremeOdrzavanja).ToList();
                Session["vreme"] = 0;
            }

            ViewBag.korisnik = posetilac;

            //  Dodajemo sta nam treba na frontu
            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.posetilac = posetilac;
            return View("MojiGrupniTreninzi");
        }

        [HttpPost]
        public ActionResult Pretrazi(string naziv, string tip, string nazivFC)
        {
            Session["PosetilacNaziv"] = naziv;
            Session["PosetilacTip"] = tip;
            Session["PosetilacNazivFC"] = nazivFC;

            return RedirectToAction("Pretraga");
        }

        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Pretraga()
        {
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];
            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!posetilac.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            string naziv = (string)Session["PosetilacNaziv"];
            string tip = (string)Session["PosetilacTip"];
            string nazivFC = (string)Session["PosetilacNazivFC"];

            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in posetilac.PosecujeGrupneTreninge.ToList())
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToPosetilac(item.Id.ToString());
                if (grupnitrening != null)
                {
                    if (grupnitrening.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupnitrening);
                    }
                }
                else
                {
                    posetilac.PosecujeGrupneTreninge.Remove(item);
                }
            }

            List<GrupniTrening> sortiranitreninzipretrazeni = new List<GrupniTrening>();
            List<GrupniTrening> sortiranitreninzistari = new List<GrupniTrening>();
            foreach (var item in posetilac.PosecujeGrupneTreninge)
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToPosetilac(item.Id.ToString());

                if (grupnitrening != null && grupnitrening.VremeOdrzavanja < DateTime.Now)
                {
                    sortiranitreninzistari.Add(item);
                }
            }

            foreach(var i in sortiranitreninzistari)
            {
                if(i.Naziv.ToLower().Contains(naziv.ToLower()) && i.TipTreninga.ToString().ToLower().Contains(tip.ToLower()) && i.FitnesCentar.Naziv.ToLower().Contains(nazivFC.ToLower()))
                {
                    sortiranitreninzipretrazeni.Add(i);
                }
            }

            ViewBag.korisnik = posetilac;

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.posetilac = posetilac;
            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            Session["pretrazenitreninzi"] = sortiranitreninzipretrazeni;
            ViewBag.sortiranitreninzistari = sortiranitreninzipretrazeni;
            return View("MojiGrupniTreninzi");
        }

        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrijaviSe(int idTreninga)
        {
            GrupniTrening grupniTrening = Load.GrupniTreningFromDataBaseToTrener(idTreninga.ToString());
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];

            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (grupniTrening == null)
            {
                Session["AlertMessage"] = "Izabrani grupni trening vise ne postoji!";
                return RedirectToAction("MojiGrupniTreninzi");
            }

            if (grupniTrening.Posetioci.Count >= grupniTrening.MaksimalanBrojPosetilaca)
            {
                Session["AlertMessage"] = "Neuspesna prijava, grupni trening je pun!"; 
                return RedirectToAction("MojiGrupniTreninzi");
            }

            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBase();

            int index = posetilac.PosecujeGrupneTreninge.FindIndex(f => f.Id == idTreninga);
            if(index >= 0)
            {
                Session["AlertMessage"] = "Neuspesna prijava, vec ste prijavljeni na ovaj grupni trening!";
                return RedirectToAction("MojiGrupniTreninzi");
            }

            // Dodajemo trening u sesiju
            posetilac.PosecujeGrupneTreninge.Add(grupniTrening);

            // Dodajemo trening u fajl Korisniku
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml"));
            xmlDoc.Load(path);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach (XmlNode node in nodeList)
            {
                if (node.SelectSingleNode("Id").InnerText == posetilac.Id.ToString())
                {
                    XmlNode root = node.SelectSingleNode("GrupniTreninzi");
                    XmlElement element = xmlDoc.CreateElement("GrupniTrening");
                    element.InnerText = idTreninga.ToString();
                    root.AppendChild(element);
                }
            }
            xmlDoc.Save(path);


            // Dodajemo posetioca u fajl Treninga
            xmlDoc = new XmlDocument();
            path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml"));
            xmlDoc.Load(path);
            nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

            foreach (XmlNode node in nodeList)
            {
                if (node.SelectSingleNode("Id").InnerText == idTreninga.ToString())
                {
                    XmlNode root = node.SelectSingleNode("Posetioci");
                    XmlElement element = xmlDoc.CreateElement("Posetilac");
                    element.InnerText = posetilac.Id.ToString();
                    root.AppendChild(element);
                }
            }
            xmlDoc.Save(path);

            List<GrupniTrening> sortiranitreninzistari = new List<GrupniTrening>();
            List<GrupniTrening> sortiranitreninzinovi = new List<GrupniTrening>();
            foreach (var item in posetilac.PosecujeGrupneTreninge.ToList())
            {
                GrupniTrening grupnitrening = Load.GrupniTreningFromDataBaseToPosetilac(item.Id.ToString());
                if (grupnitrening != null)
                {
                    if (grupnitrening.VremeOdrzavanja > DateTime.Now)
                    {
                        sortiranitreninzinovi.Add(grupnitrening);
                    }
                    else
                    {
                        sortiranitreninzistari.Add(grupnitrening);
                    }
                }
                else
                {
                    posetilac.PosecujeGrupneTreninge.Remove(item);
                }
            }

            sortiranitreninzistari = sortiranitreninzistari.OrderBy(l => l.VremeOdrzavanja).ToList();
            sortiranitreninzinovi = sortiranitreninzinovi.OrderBy(l => l.VremeOdrzavanja).ToList();

            Session["AlertMessage"] = "Uspesna prijava na grupni trening pod nazivom: " + grupniTrening.Naziv;

            ViewBag.tipovitreninga = Enum.GetValues(typeof(TipTreninga));
            ViewBag.sortiranitreninzinovi = sortiranitreninzinovi;
            ViewBag.sortiranitreninzistari = sortiranitreninzistari;
            ViewBag.korisnik = posetilac;
            ViewBag.posetilac = posetilac;
            return RedirectToAction("MojiGrupniTreninzi");
        }

        [HttpPost]
        public ActionResult Ostavi(int idFC)
        {
            Session["id"] = idFC;

            return RedirectToAction("OstaviKomentar");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult OstaviKomentar()
        {
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];
            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!posetilac.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            int idFC = (int)Session["id"];

            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();

            if (!fitnescentri.ContainsKey(idFC))
            {
                Session["AlertMessage"] = "Nazalost, ovaj fitnes centar je u medjuvremenu obrisan!";
                return RedirectToAction("MojiGrupniTreninzi");
            }

            ViewBag.korisnik = posetilac;

            ViewBag.fitnescentar = fitnescentri[idFC];
            return View();
        }

        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult KomentarDodat(int id,string komentar, int ocena)
        {
            Korisnik posetilac = (Korisnik)Session["PrijavljeniKorisnik"];
            if (posetilac == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (posetilac.Uloga != Uloga.POSETILAC)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!posetilac.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            if (komentar == "" || komentar == null || ocena == 0)
            {
                Session["AlertMessage"] = "Sva polja moraju biti popunjena!";
                return RedirectToAction("Index", "Home");
            }

            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();
            if (!fitnescentri.ContainsKey(id))
            {
                Session["AlertMessage"] = "Nazalost, ovaj fitnes centar je u medjuvremenu obrisan!";
                return RedirectToAction("MojiGrupniTreninzi");
            }

            List<Komentar> komentari = Load.KomenteriFromDataBaseObrisani();

            Komentar kom = new Komentar();
            kom.Id = komentari.Count;
            kom.Tekst = komentar;
            kom.Ocena = ocena;
            kom.Posetilac.Id = posetilac.Id;
            kom.FitnesCentar.Id = id;
            kom.Obrisan = false;
            kom.Odobren = false;

            Write.KomentarToDataBase(kom);


            Session["AlertMessage"] = "Komentar uspesno poslat. Ceka se odobrenje vlasnika!";
            return RedirectToAction("MojiGrupniTreninzi");
        }
    }
}