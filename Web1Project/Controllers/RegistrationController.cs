﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Web1Project.Models;

namespace Web1Project.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: Registration
        public ActionResult Index()
        {
            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }

            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            ViewBag.korisnik = korisnik;

            return View();
        }

        [HttpPost]
        public JsonResult CheckUserAvailability(string userdata)
        {
            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBaseObrisani();

            if (korisnici.ContainsKey(userdata))
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        public JsonResult EmailValidation(string email)
        {
            bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            if (isEmail)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        [HttpPost]
        public ActionResult Add(Korisnik korisnik)
        {
            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBaseObrisani();

            if (korisnik.KorisnickoIme == null || korisnik.KorisnickoIme == "" || korisnik.Ime == null || korisnik.Ime == "" || korisnik.Prezime == null || korisnik.Prezime == "" || korisnik.Lozinka == null || korisnik.Lozinka == "" || korisnik.Email == null || korisnik.Email == "" || korisnik.DatumRodjenja > DateTime.Now)
            {
                Session["AlertMessage"] = "Sva polja moraju biti popunjena";
                return RedirectToAction("Index");
            }

            if (korisnici.ContainsKey(korisnik.KorisnickoIme))
            {
                Session["AlertMessage"] = "Korisnicko ime je zauzeto";
                return RedirectToAction("Index");
            }

            bool isEmail = Regex.IsMatch(korisnik.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            if (isEmail)
            {
                korisnik.Id = korisnici.Count;
                korisnik.PosecujeGrupneTreninge = new List<GrupniTrening>();

                korisnik.Lozinka = Operations.hashPassword(korisnik.Lozinka);

                Write.PosetilacToDataBase(korisnik);

                Session["AlertMessage"] = "Korisnik uspesno registrovan!";                
                return RedirectToAction("Index", "Home");
            }
            else
            {
                Session["AlertMessage"] = "Email nije validan";
                return RedirectToAction("Index");
            }
        }
    }
}