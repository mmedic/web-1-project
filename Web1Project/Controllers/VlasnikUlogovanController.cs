﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using Web1Project.Models;

namespace Web1Project.Controllers
{
    public class VlasnikUlogovanController : Controller
    {
        // GET: VlasnikUlogovan 
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult MojiFitnesCentri()
        {
            Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (vlasnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }else if (vlasnik.Uloga != Uloga.VLASNIK)
            {
                return RedirectToAction("Index", "Home");
            }


            if (!vlasnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();

            List<FitnesCentar> sortiranaLista = new List<FitnesCentar>();

            foreach (var item in fitnescentri)
            {
                if(vlasnik.Id == item.Value.Vlasnik.Id)
                {
                    sortiranaLista.Add(item.Value);
                }
            }

            sortiranaLista = sortiranaLista.OrderBy(l => l.Naziv).ToList();


            ViewBag.sortiranaLista = sortiranaLista;
            ViewBag.korisnik = vlasnik;
            ViewBag.vlasnik = vlasnik;
            ViewBag.AlertMessage = "";

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }
            return View();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult DodajFitnesCentar()
        {
            Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (vlasnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (vlasnik.Uloga != Uloga.VLASNIK)
            {
                return RedirectToAction("Index", "Home");
            }


            if (!vlasnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }
            ViewBag.korisnik = vlasnik;

            return View();
        }

        [HttpPost]
        public ActionResult FitnesCentarDodat(string naziv, string adresa)
        {
            Dictionary<int, FitnesCentar> fitnescentriObrisani = Load.FitnesCentriFromDataBaseObrisani();

            string godinaotvaranjaS = Request["godinaotvaranja"] != null ? Request["godinaotvaranja"] : string.Empty;
            int.TryParse(godinaotvaranjaS, out int godinaotvaranja);

            string cenamesecneclanarineS = Request["cenamesecneclanarine"] != null ? Request["cenamesecneclanarine"] : string.Empty;
            int.TryParse(cenamesecneclanarineS, out int cenamesecneclanarine);

            string cenagodisnjeclanarineS = Request["cenagodisnjeclanarine"] != null ? Request["cenagodisnjeclanarine"] : string.Empty;
            int.TryParse(cenagodisnjeclanarineS, out int cenagodisnjeclanarine);

            string cenajednogtreningaS = Request["cenajednogtreninga"] != null ? Request["cenajednogtreninga"] : string.Empty;
            int.TryParse(cenajednogtreningaS, out int cenajednogtreninga);

            string cenajednoggrupnogS = Request["cenajednoggrupnog"] != null ? Request["cenajednoggrupnog"] : string.Empty;
            int.TryParse(cenajednoggrupnogS, out int cenajednoggrupnog);

            string cenajednogptS = Request["cenajednogpt"] != null ? Request["cenajednogpt"] : string.Empty;
            int.TryParse(cenajednogptS, out int cenajednogpt);

            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();
            bool postoji = false;
            foreach(var i in fitnescentri)
            {
                if(i.Value.Naziv.ToLower() == naziv.ToLower())
                {
                    postoji = true;
                    break;
                }
            }

            if(!postoji && naziv != "" && adresa != ""  && godinaotvaranja != 0 && cenamesecneclanarine != 0 && cenagodisnjeclanarine != 0 && cenajednogtreninga != 0 && cenajednoggrupnog != 0 && cenajednogpt != 0)
            {
                Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];

                FitnesCentar noviFitnesCentar = new FitnesCentar(vlasnik);

                noviFitnesCentar.Id = fitnescentriObrisani.Count;
                noviFitnesCentar.Naziv = naziv;
                noviFitnesCentar.Adresa = adresa;
                noviFitnesCentar.GodinaOtvaranja = godinaotvaranja;
                noviFitnesCentar.CenaMesecneClanarine = cenamesecneclanarine;
                noviFitnesCentar.CenaGodisnjeClanarine = cenagodisnjeclanarine;
                noviFitnesCentar.CenaJednogTreninga = cenajednogtreninga;
                noviFitnesCentar.CenaJednogGrupnog = cenajednoggrupnog;
                noviFitnesCentar.CenaJednogPT = cenajednogpt;

                vlasnik.FitnesCentriPoseduje.Add(noviFitnesCentar);

                Write.FitnesCentarToDataBase(noviFitnesCentar);

                // Dodaje sebi FitnesCentar u vlasnistvo
                XmlDocument xmlDoc = new XmlDocument();
                string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml"));
                xmlDoc.Load(path);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

                foreach (XmlNode node in nodeList)
                {
                    if (node.SelectSingleNode("Id").InnerText == vlasnik.Id.ToString())
                    {
                        XmlNode root = node.SelectSingleNode("FitnesCentri");
                        XmlElement element = xmlDoc.CreateElement("FitnesCentar");
                        element.InnerText = noviFitnesCentar.Id.ToString();
                        root.AppendChild(element);
                    }
                }
                xmlDoc.Save(path);

                Session["AlertMessage"] = "Fitnes centar uspesno dodat!";
                return RedirectToAction("MojiFitnesCentri");
            }

            Session["AlertMessage"] = "Sva polja moraju biti pravilno popunjena";
            return RedirectToAction("DodajFitnesCentar");
        }
              
        [HttpPost]
        public ActionResult Izmeni(int id)
        { 
            Session["id"] = id;
            return RedirectToAction("IzmeniFitnesCentar");
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult IzmeniFitnesCentar()
        {
            Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (vlasnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (vlasnik.Uloga != Uloga.VLASNIK)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!vlasnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            int id = (int)Session["id"];
            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();

            ViewBag.korisnik = vlasnik;

            ViewBag.fitnescentar = fitnescentri[id];
            return View();
        }

        [HttpPost]
        public ActionResult FitnesCentarIzmenjen(int id, string novinaziv, string novaadresa)
        {
            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();       
            
            string novacenamesecneclanarineString = Request["novacenamesecneclanarine"] != null ? Request["novacenamesecneclanarine"] : string.Empty;
            string novacenagodisnjeclanarineString = Request["novacenagodisnjeclanarine"] != null ? Request["novacenagodisnjeclanarine"] : string.Empty;
            string novacenajednogtreningaString = Request["novacenajednogtreninga"] != null ? Request["novacenajednogtreninga"] : string.Empty;
            string novacenajednoggrupnogString = Request["novacenajednoggrupnog"] != null ? Request["novacenajednoggrupnog"] : string.Empty;
            string novacenajednogptString = Request["novacenajednogpt"] != null ? Request["novacenajednogpt"] : string.Empty;

            bool flag = false;

            if(novinaziv != "" || novaadresa != "" || novacenamesecneclanarineString != "" || novacenagodisnjeclanarineString != "" || novacenajednogtreningaString != "" || novacenajednoggrupnogString != "" || novacenajednogptString != "")
            {
                XmlDocument xmlDoc = new XmlDocument();
                string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/fitnescentri.xml"));
                xmlDoc.Load(path);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/FitnesCentri/FitnesCentar");

                FitnesCentar fitnescentar = fitnescentri[id];

                if(novinaziv != "" && novinaziv != fitnescentar.Naziv)
                {
                    foreach(var v in fitnescentri)
                    {
                        if(v.Value.Naziv == novinaziv)
                        {
                            Session["AlertMessage"] = "Fitnes centar sa ovim nazivom vec postoji!";
                            return RedirectToAction("MojiFitnesCentri");
                        }
                    }
                    foreach(XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            fitnescentar.Naziv = novinaziv;

                            node.SelectSingleNode("Naziv").InnerText = novinaziv;
                            break;
                        }
                    }
                }
                if (novaadresa != "" && novaadresa != fitnescentar.Adresa)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            fitnescentar.Adresa = novaadresa;

                            node.SelectSingleNode("Adresa").InnerText = novaadresa;
                            break;
                        }
                    }
                }
                if (novacenamesecneclanarineString != "" && int.Parse(novacenamesecneclanarineString) != fitnescentar.CenaMesecneClanarine)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            fitnescentar.CenaMesecneClanarine = int.Parse(novacenamesecneclanarineString);

                            node.SelectSingleNode("CenaMesecneClanarine").InnerText = novacenamesecneclanarineString;
                            break;
                        }
                    }
                }
                if (novacenagodisnjeclanarineString != "" && int.Parse(novacenagodisnjeclanarineString) != fitnescentar.CenaGodisnjeClanarine)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            fitnescentar.CenaGodisnjeClanarine = int.Parse(novacenagodisnjeclanarineString);

                            node.SelectSingleNode("CenaGodisnjeClanarine").InnerText = novacenagodisnjeclanarineString;
                            break;
                        }
                    }
                }
                if (novacenajednogtreningaString != "" && int.Parse(novacenajednogtreningaString) != fitnescentar.CenaJednogTreninga)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            fitnescentar.CenaJednogTreninga = int.Parse(novacenajednogtreningaString);

                            node.SelectSingleNode("CenaJednogTreninga").InnerText = novacenajednogtreningaString;
                            break;
                        }
                    }
                }
                if (novacenajednoggrupnogString != "" && int.Parse(novacenajednoggrupnogString) != fitnescentar.CenaJednogGrupnog)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            fitnescentar.CenaJednogGrupnog = int.Parse(novacenajednoggrupnogString);

                            node.SelectSingleNode("CenaJednogGrupnog").InnerText = novacenajednoggrupnogString;
                            break;
                        }
                    }
                }
                if (novacenajednogptString != "" && int.Parse(novacenajednogptString) != fitnescentar.CenaJednogPT)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                        {
                            flag = true;
                            fitnescentar.CenaJednogPT = int.Parse(novacenajednogptString);

                            node.SelectSingleNode("CenaJednogPT").InnerText = novacenajednogptString;
                            break;
                        }
                    }
                }

                if (flag)
                {
                    xmlDoc.Save(path);
                    Session["AlertMessage"] = "Fitnes centar uspesno izmenjen!";

                    Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
                    if (!vlasnik.LogedIn)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    for(int i = 0; i < vlasnik.FitnesCentriPoseduje.Count; i++)
                    {
                        if(vlasnik.FitnesCentriPoseduje[i].Id == id)
                        {
                            vlasnik.FitnesCentriPoseduje[i] = fitnescentar;
                            break;
                        }
                    }
                    Session["PrijavljeniKorisnik"] = vlasnik;
                }
                else
                {
                    Session["AlertMessage"] = "Fitnes centar nije izmenjen";
                }                
            }
            
            return RedirectToAction("MojiFitnesCentri");
        }

        [HttpPost]
        public ActionResult ObrisiFitnesCentar(int id)
        {
            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBase();
            Dictionary<int, GrupniTrening> grupnitreninzi = Load.GrupniTreninziFromDataBase();

            foreach(var trening in grupnitreninzi)
            {
                if(trening.Value.FitnesCentar.Id == id)
                {
                    if(trening.Value.VremeOdrzavanja > DateTime.Now)
                    {
                        Session["AlertMessage"] = "Ovaj fitnes cenar ne moze biti obrisan";
                        return RedirectToAction("MojiFitnesCentri");
                    }
                }
            }

            Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
            foreach(var i in vlasnik.FitnesCentriPoseduje)
            {
                if(i.Id == id)
                {
                    vlasnik.FitnesCentriPoseduje.Remove(i);
                    break;
                }
            }


            Session["AlertMessage"] = "Fitnes centar uspesno obrisan";

            // brisemo fitnes centar iz fajla
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/fitnescentri.xml"));
            xmlDoc.Load(path);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/FitnesCentri/FitnesCentar");

            foreach(XmlNode node in nodeList)
            {
                if (int.Parse(node.SelectSingleNode("Id").InnerText) == id)
                {
                    node.SelectSingleNode("Obrisan").InnerText = "true";
                    break;
                }
            }

            xmlDoc.Save(path);

            // brisemo trenere iz fajla
            xmlDoc = new XmlDocument();
            path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml"));
            xmlDoc.Load(path);
            nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach (var item in korisnici)
            {
                if(item.Value.Uloga == Uloga.TRENER)
                {
                    if (item.Value.FitnesCentarZaposlen.Id == id)
                    {
                        item.Value.Blokiran = true;

                        foreach (XmlNode node in nodeList)
                        {
                            if (node.SelectSingleNode("Id").InnerText == item.Value.Id.ToString())
                            {
                                node.SelectSingleNode("Blokiran").InnerText = "true";
                                break;
                            }
                        }
                    }
                }
            }
            xmlDoc.Save(path);

            // brisemo grupne treninge iz fajla
            xmlDoc = new XmlDocument();
            path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/grupnitreninzi.xml"));
            xmlDoc.Load(path);
            nodeList = xmlDoc.DocumentElement.SelectNodes($"/GrupniTreninzi/GrupniTrening");

            foreach(XmlNode node in nodeList)
            {
                if(node.SelectSingleNode("FitnesCentar").InnerText == id.ToString())
                {
                    node.SelectSingleNode("Obrisan").InnerText = "true";
                }
            }
            xmlDoc.Save(path);


            // brisemo komentare iz fajla
            xmlDoc = new XmlDocument();
            path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/komentari.xml"));
            xmlDoc.Load(path);
            nodeList = xmlDoc.DocumentElement.SelectNodes($"/Komentari/Komentar");

            foreach(XmlNode node in nodeList)
            {
                if(node.SelectSingleNode("FitnesCentar").InnerText == id.ToString())
                {
                    node.SelectSingleNode("Obrisan").InnerText = "true";
                }
            }
            xmlDoc.Save(path);

            return RedirectToAction("MojiFitnesCentri");
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult MojiTreneri()
        {
            Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (vlasnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (vlasnik.Uloga != Uloga.VLASNIK)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!vlasnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBase();
            List<Korisnik> njegoviTreneri = new List<Korisnik>();

            foreach(var korisnik in korisnici)
            {
                if(korisnik.Value.Uloga == Uloga.TRENER)
                {
                    for (int i = 0; i < vlasnik.FitnesCentriPoseduje.Count;i++)
                    {
                        if(korisnik.Value.FitnesCentarZaposlen.Id == vlasnik.FitnesCentriPoseduje[i].Id)
                        {
                            njegoviTreneri.Add(korisnik.Value);
                        }
                    }
                }
            }

            ViewBag.korisnik = vlasnik;
            ViewBag.treneri = njegoviTreneri;

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }
            return View();
        }

        public ActionResult Blokiraj(int id)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/korisnici.xml"));
            xmlDoc.Load(path);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

            foreach(XmlNode node in nodeList)
            {
                if(node.SelectSingleNode("Id").InnerText == id.ToString())
                {
                    node.SelectSingleNode("Blokiran").InnerText = "true";
                    break;
                }
            }

            xmlDoc.Save(path);

            Session["AlertMessage"] = "Trener uspesno blokiran!";
            return RedirectToAction("MojiTreneri");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult DodajTrenera()
        {
            Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (vlasnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (vlasnik.Uloga != Uloga.VLASNIK)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!vlasnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.fitnescentri = vlasnik.FitnesCentriPoseduje;

            ViewBag.korisnik = vlasnik;


            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }
            return View();
        }

        [HttpPost]
        public ActionResult TrenerDodat(Korisnik trener)
        {
            string datumRodjenja = Request["DatumRodjenja"] != null ? Request["DatumRodjenja"] : string.Empty;

            if (trener.KorisnickoIme != null && trener.Lozinka != null && trener.Ime != null && trener.Prezime != null && trener.Email !=null && datumRodjenja != "")
            {
                Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBaseObrisani();
                Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();

                string naziv = Request["naziv"] != null ? Request["naziv"] : string.Empty;

                bool postoji = false;
                foreach(var centar in fitnescentri)
                {
                    if(centar.Value.Naziv == naziv)
                    {
                        postoji = true;
                    }
                }

                if (!postoji)
                {
                    Session["AlertMessage"] = "Izabrani fitnes centar je u medjuvremenu obrisan ili izmenjen!";
                    return RedirectToAction("MojiFitnesCentri");
                }


                bool isEmail = Regex.IsMatch(trener.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

                if (isEmail && trener.DatumRodjenja < DateTime.Now && !korisnici.ContainsKey(trener.KorisnickoIme))
                {
                    trener.Id = korisnici.Count;
                    trener.Uloga = Uloga.TRENER;

                    foreach (var item in fitnescentri)
                    {
                        if (item.Value.Naziv == naziv && !item.Value.Obrisan)
                        {
                            trener.FitnesCentarZaposlen = item.Value;
                            break;
                        }
                    }

                    trener.Lozinka = Operations.hashPassword(trener.Lozinka);

                    Write.TrenerToDataBase(trener);

                    Session["AlertMessage"] = "Trener uspesno dodat!";
                    return RedirectToAction("MojiTreneri");
                }
            }
            Session["AlertMessage"] = "Sva polja moraju biti pravilno popunjena";
            return RedirectToAction("DodajTrenera");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult OdobriKomentare()
        {
            Korisnik vlasnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (vlasnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            else if (vlasnik.Uloga != Uloga.VLASNIK)
            {
                return RedirectToAction("Index", "Home");
            }


            if (!vlasnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }

            List<Komentar> sviKomentari = Load.KomenteriFromDataBase();
            List<Komentar> njegoviKomentari = new List<Komentar>();

            foreach(var kom in sviKomentari)
            {
                int index = vlasnik.FitnesCentriPoseduje.FindIndex(k => k.Id == kom.FitnesCentar.Id);

                if(index >= 0 && !kom.Odobren)
                {
                    njegoviKomentari.Add(kom);
                }
            }

            ViewBag.komentari = njegoviKomentari;

            ViewBag.korisnik = vlasnik;

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }
            return View();
        }

        [HttpPost]
        public ActionResult KomentarOdobren(int id)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/komentari.xml"));
            xmlDoc.Load(path);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Komentari/Komentar");

            foreach(XmlNode node in nodeList)
            {
                if(node.SelectSingleNode("Id").InnerText == id.ToString())
                {
                    node.SelectSingleNode("Odobren").InnerText = "true";
                }
            }

            xmlDoc.Save(path);


            Session["AlertMessage"] = "Komentar odobren!";
            return RedirectToAction("OdobriKomentare");
        }

        [HttpPost]
        public ActionResult KomentarOdbijen(int id)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = Path.Combine(HostingEnvironment.MapPath("~/App_Data/komentari.xml"));
            xmlDoc.Load(path);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Komentari/Komentar");

            foreach (XmlNode node in nodeList)
            {
                if (node.SelectSingleNode("Id").InnerText == id.ToString())
                {
                    node.SelectSingleNode("Obrisan").InnerText = "true";
                }
            }

            xmlDoc.Save(path);

            Session["AlertMessage"] = "Komentar odbijen!";
            return RedirectToAction("OdobriKomentare");
        }

        [HttpPost]
        public JsonResult CheckUserAvailability(string userdata)
        {
            Dictionary<int, FitnesCentar> fitnesCentri = Load.FitnesCentriFromDataBase();

            bool postoji = false;

            foreach(var centar in fitnesCentri)
            {
                if(centar.Value.Naziv == userdata)
                {
                    postoji = true;
                    break;
                }
            }

            if (postoji)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }
    }
}