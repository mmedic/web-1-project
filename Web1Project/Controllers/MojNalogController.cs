﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Xml;
using Web1Project.Models;

namespace Web1Project.Controllers
{
    public class MojNalogController : Controller
    {
        // GET: MojNalog
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if(korisnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }

            if (!korisnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }

            ViewBag.korisnik = korisnik;

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }

            return View();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult LicniPodaci()
        {
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (korisnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }

            if (!korisnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }



            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            else
            {
                ViewBag.AlertMessage = null;
            }

            ViewBag.korisnik = korisnik;

            return View();
        }

        [HttpPost]
        public ActionResult LicniPodaciPromenjeni(string korisnickoime, string ime, string prezime)
        {
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBaseObrisani();

            bool flag = false;

            if (korisnickoime != "" || ime != "" || prezime != "")
            {
                XmlDocument xmlDoc = new XmlDocument();
                string path = Path.Combine(Server.MapPath("~/App_Data/korisnici.xml"));
                xmlDoc.Load(path);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

                if (ime != "" && ime != korisnik.Ime)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.SelectSingleNode("KorisnickoIme").InnerText == korisnik.KorisnickoIme)
                        {
                            flag = true;
                            node.SelectSingleNode("Ime").InnerText = ime;

                            korisnik.Ime = ime;
                            break;
                        }
                    }
                }
                if (prezime != "" && prezime!=korisnik.Prezime)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        if (node.SelectSingleNode("KorisnickoIme").InnerText == korisnik.KorisnickoIme)
                        {
                            flag = true;
                            node.SelectSingleNode("Prezime").InnerText = prezime;

                            korisnik.Prezime = prezime;
                            break;
                        }
                    }
                }

                if (korisnickoime != "" && korisnickoime!=korisnik.KorisnickoIme)
                {
                    if (korisnici.ContainsKey(korisnickoime))
                    {
                        Session["AlertMessage"] = "Ovo korisnicko ime je zauzeto!";
                        return RedirectToAction("LicniPodaci");
                    }
                    else
                    {
                        foreach (XmlNode node in nodeList)
                        {
                            if (node.SelectSingleNode("KorisnickoIme").InnerText == korisnik.KorisnickoIme)
                            {
                                flag = true;
                                node.SelectSingleNode("KorisnickoIme").InnerText = korisnickoime;

                                korisnik.KorisnickoIme = korisnickoime;
                                break;
                            }
                        }
                    }
                }

                if (flag)
                {
                    xmlDoc.Save(path);  
                    Session["AlertMessage"] = "Licni podaci uspesno izmenjeni!";

                    Session["PrijavljeniKorisnik"] = korisnik;
                }
                else
                {
                    Session["AlertMessage"] = "Licni podaci nisu izmenjeni!";
                }            
            }          
            return RedirectToAction("Index");
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Email()
        {
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (korisnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            if (!korisnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }

            ViewBag.Message1 = "";
            ViewBag.Message2 = "";
            ViewBag.korisnik = korisnik;

            return View();
        }

        [HttpPost]
        public ActionResult EmailPromenjen(string email, string lozinka)
        {
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];

            if (email != "" && email != korisnik.Email)
            {
                bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

                lozinka = Operations.hashPassword(lozinka);

                if (lozinka == korisnik.Lozinka && isEmail)
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    string path = Path.Combine(Server.MapPath("~/App_Data/korisnici.xml"));
                    xmlDoc.Load(path);
                    XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

                    foreach (XmlNode node in nodeList)
                    {
                        if (node.SelectSingleNode("KorisnickoIme").InnerText == korisnik.KorisnickoIme)
                        {
                            node.SelectSingleNode("Email").InnerText = email;

                            korisnik.Email = email;

                            xmlDoc.Save(path);
                            Session["PrijavljeniKorisnik"] = korisnik;
                            break;
                        }
                    }
                    Session["AlertMessage"] = "Email uspesno izmenjen!"; 
                }
                else
                {
                    if (!isEmail)
                    {
                        ViewBag.Message1 = "Email nije validan";
                    }

                    if (lozinka != korisnik.Lozinka)
                    {
                        ViewBag.Message2 = "Neispravna lozinka";
                    }

                    ViewBag.korisnik = korisnik;
                    return View("Email");
                }
            }
            ViewBag.korisnik = korisnik;
            return RedirectToAction("Index");
        }
      
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Lozinka()
        {
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            if (korisnik == null)
            {
                return RedirectToAction("Index", "Authentication");
            }
            if (!korisnik.LogedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }

            ViewBag.Message1 = "";
            ViewBag.Message2 = "";

            ViewBag.korisnik = korisnik;

            return View();
        }

        [HttpPost]
        public ActionResult LozinkaPromenjena(string lozinka, string novalozinka, string novalozinkaponovo)
        {
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];

            if (lozinka != "" && novalozinka != "" && novalozinkaponovo != "")
            {
                lozinka = Operations.hashPassword(lozinka);

                if(korisnik.Lozinka == lozinka && novalozinka == novalozinkaponovo)
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    string path = Path.Combine(Server.MapPath("~/App_Data/korisnici.xml"));
                    xmlDoc.Load(path);
                    XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes($"/Korisnici/Korisnik");

                    foreach (XmlNode node in nodeList)
                    {
                        if (node.SelectSingleNode("KorisnickoIme").InnerText == korisnik.KorisnickoIme)
                        {
                            novalozinka = Operations.hashPassword(novalozinka);

                            node.SelectSingleNode("Lozinka").InnerText = novalozinka; 

                            korisnik.Lozinka = novalozinka;

                            xmlDoc.Save(path);
                            Session["PrijavljeniKorisnik"] = korisnik;
                            break;
                        }
                    }
                    Session["AlertMessage"] = "Lozinka uspesno izmenjena!";
                }
                else
                {
                    if(korisnik.Lozinka != lozinka)
                    {
                        ViewBag.Message1 = "Neispravna lozinka";
                    }

                    if(novalozinka != novalozinkaponovo)
                    {
                        ViewBag.Message2 = "Ponovljena lozinka se ne poklapa";
                    }
                    ViewBag.korisnik = korisnik;
                    return View("Lozinka");
                }
            }

            ViewBag.korisnik = korisnik;
            return RedirectToAction("Index");
        }
    }
}