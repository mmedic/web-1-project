﻿using System.Collections.Generic;
using System.Web.Mvc;
using Web1Project.Models;

namespace Web1Project.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            ViewBag.Ime = "";
            ViewBag.Message = "";

            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            ViewBag.korisnik = korisnik;


            return View();
        }

        [HttpPost]
        public ActionResult Login(string korisnickoIme, string lozinka)
        {
            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBase();

            if (korisnickoIme == "" || korisnickoIme == null || lozinka == "" || lozinka == null)
            {
                Session["AlertMessage"] = "Sva polja moraju biti popunjena";
                ViewBag.korisnik = new Korisnik();
                return View("Index");
            }

            lozinka = Operations.hashPassword(lozinka);

            if (korisnici.ContainsKey(korisnickoIme))
            {
                if(korisnici[korisnickoIme].Lozinka != lozinka)
                {
                    ViewBag.Ime = korisnickoIme;
                    ViewBag.korisnik = new Korisnik();

                    ViewBag.Message = "Pogresna lozinka";
                    return View("Index");
                }
            }
            else
            {
                Session["AlertMessage"] = "Ovo korisnicko ime ne postoji";
                ViewBag.korisnik = new Korisnik();
                return View("Index");
            }

            korisnici[korisnickoIme].LogIn();   
            Session["PrijavljeniKorisnik"] = korisnici[korisnickoIme];

            Session["AlertMessage"] = "Korisnik: " + korisnickoIme + " uspesno ulogovan!";
            return RedirectToAction("Index","Home");
        }

        public ActionResult Logout()
        {
            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBaseObrisani();
            Korisnik korisnik = (Korisnik)Session["PrijavljeniKorisnik"];

            korisnici[korisnik.KorisnickoIme].LogOut();
            Session["PrijavljeniKorisnik"] = new Korisnik();

            if ((string)Session["AlertMessage"] != "Blokirani ste iz sistema!")
            {
                Session["AlertMessage"] = "Uspesno ste se odjavili!";
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public JsonResult CheckUserAvailability(string userdata)
        {
            Dictionary<string, Korisnik> korisnici = Load.KorisnikFromDataBase();

            if (korisnici.ContainsKey(userdata))
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }
    }
}