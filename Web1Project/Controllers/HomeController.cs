﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Web1Project.Models;

namespace Web1Project.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();
            Session["pretrazenicentri"] = fitnescentri;

            List<FitnesCentar> fitnesCentriSortiraniPoNazivu = fitnescentri.Values.OrderBy(l => l.Naziv).ToList();           

            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            if(korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }
                 
            Session["naziv"] = 1;
            Session["adresa"] = 0;
            Session["godina"] = 0;         

            ViewBag.sortiranaLista = fitnesCentriSortiraniPoNazivu;
            ViewBag.korisnik = korisnik;

            if (Session["AlertMessage"] != null)
            {
                ViewBag.AlertMessage = Session["AlertMessage"];
                Session["AlertMessage"] = null;
            }
            return View();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Naziv()
        {

            Dictionary<int, FitnesCentar> fitnescentri = new Dictionary<int, FitnesCentar>();

            Dictionary<int, FitnesCentar> fitnescentriPretrazeni = (Dictionary<int, FitnesCentar>)Session["pretrazenicentri"];
            Dictionary<int, FitnesCentar> fitnescentriUBazi = Load.FitnesCentriFromDataBase();

            foreach(var centar in fitnescentriPretrazeni)
            {
                if (fitnescentriUBazi.ContainsKey(centar.Key))
                {
                    fitnescentri.Add(centar.Key, fitnescentriUBazi[centar.Key]);
                }
            }

            List<FitnesCentar> sortiranaLista;

            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }
            ViewBag.korisnik = korisnik;

            Session["adresa"] = 0;
            Session["godina"] = 0;

            if ((int)Session["naziv"] == 0)
            {
                sortiranaLista = fitnescentri.Values.OrderBy(l => l.Naziv).ToList();
                Session["naziv"] = 1;
            }
            else
            {
                sortiranaLista = fitnescentri.Values.OrderByDescending(l => l.Naziv).ToList();
                Session["naziv"] = 0;
            }

            
            ViewBag.sortiranaLista = sortiranaLista;
            return View("Index");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Adresa()
        {
            Dictionary<int, FitnesCentar> fitnescentri = new Dictionary<int, FitnesCentar>();

            Dictionary<int, FitnesCentar> fitnescentriPretrazeni = (Dictionary<int, FitnesCentar>)Session["pretrazenicentri"];
            Dictionary<int, FitnesCentar> fitnescentriUBazi = Load.FitnesCentriFromDataBase();

            foreach (var centar in fitnescentriPretrazeni)
            {
                if (fitnescentriUBazi.ContainsKey(centar.Key))
                {
                    fitnescentri.Add(centar.Key, fitnescentriUBazi[centar.Key]);
                }
            }
            List<FitnesCentar> sortiranaLista;

            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }
            ViewBag.korisnik = korisnik;

            Session["naziv"] = 0;
            Session["godina"] = 0;

            if ((int)Session["adresa"] == 0)
            {
                sortiranaLista = fitnescentri.Values.OrderBy(l => l.Adresa).ToList();
                Session["adresa"] = 1;
            }
            else
            {
                sortiranaLista = fitnescentri.Values.OrderByDescending(l => l.Adresa).ToList();
                Session["adresa"] = 0;
            }

            
            ViewBag.sortiranaLista = sortiranaLista;
            return View("Index");
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Godina()
        {
            Dictionary<int, FitnesCentar> fitnescentri = new Dictionary<int, FitnesCentar>();

            Dictionary<int, FitnesCentar> fitnescentriPretrazeni = (Dictionary<int, FitnesCentar>)Session["pretrazenicentri"];
            Dictionary<int, FitnesCentar> fitnescentriUBazi = Load.FitnesCentriFromDataBase();

            foreach (var centar in fitnescentriPretrazeni)
            {
                if (fitnescentriUBazi.ContainsKey(centar.Key))
                {
                    fitnescentri.Add(centar.Key, fitnescentriUBazi[centar.Key]);
                }
            }

            List<FitnesCentar> sortiranaLista;

            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }

            ViewBag.korisnik = korisnik;

            Session["naziv"] = 0;
            Session["adresa"] = 0;

            if ((int)Session["godina"] == 0)
            {
                sortiranaLista = fitnescentri.Values.OrderBy(l => l.GodinaOtvaranja).ToList();
                Session["godina"] = 1;
            }
            else
            {
                sortiranaLista = fitnescentri.Values.OrderByDescending(l => l.GodinaOtvaranja).ToList();
                Session["godina"] = 0;
            }

            
            ViewBag.sortiranaLista = sortiranaLista;
            return View("Index");
        }

        [HttpPost]
        public ActionResult Detalji(int id)
        {
            // Izlistavamo Fitnes centre
            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();

            if (!fitnescentri.ContainsKey(id))
            {
                Session["AlertMessage"] = "Ovaj fitnes centar je obrisan!";
                return RedirectToAction("Index");
            }

            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }
            ViewBag.korisnik = korisnik;
            

            FitnesCentar centar = fitnescentri[id];
            ViewBag.centar = centar;

            // Izlistavamo grupne treninge
            Dictionary<int, GrupniTrening> grupnitreninziUBazi = Load.GrupniTreninziFromDataBase();
            List<GrupniTrening> grupnitreninzi = new List<GrupniTrening>();

            foreach(var item in grupnitreninziUBazi)
            {
                if(item.Value.FitnesCentar.Id == id)
                {
                    if(item.Value.VremeOdrzavanja > DateTime.Now)
                    {
                        grupnitreninzi.Add(item.Value);
                    }
                }
            }

            ViewBag.grupnitreninzi = grupnitreninzi;

            // Izlistavamo komentare

            List<Komentar> komentariUBazi = new List<Komentar>();
            List<Komentar> komentari = new List<Komentar>();

            bool njegov = false;
             
            if (korisnik.Uloga == Uloga.VLASNIK)
            {
                if(fitnescentri[id].Vlasnik.Id == korisnik.Id)
                {
                    njegov = true;
                    komentariUBazi = Load.KomenteriFromDataBaseObrisani();

                    foreach (var kom in komentariUBazi)
                    {
                        int index = korisnik.FitnesCentriPoseduje.FindIndex(k => k.Id == kom.FitnesCentar.Id);

                        if (index >= 0 && kom.FitnesCentar.Id == id)
                        {
                            komentari.Add(kom);
                        }
                    }
                }
            }

            if (!njegov)
            {
                komentariUBazi = Load.KomenteriFromDataBase();

                foreach (var i in komentariUBazi)
                {
                    if (i.FitnesCentar.Id == id && i.Odobren)
                    {
                        komentari.Add(i);
                    }
                }
            }
            

            ViewBag.komentari = komentari;
            return View();
        }

        [HttpPost]
        public ActionResult Pretrazi(string naziv, string adresa)
        {
            Session["HomeNaziv"] = naziv;
            Session["HomeAdresa"] = adresa;

            string godinaMin = Request["godinaOtvaranjaMin"] != null ? Request["godinaOtvaranjaMin"] : string.Empty;
            int.TryParse(godinaMin, out int godinaOtvaranjaMin);

            string godinaMax = Request["godinaOtvaranjaMax"] != null ? Request["godinaOtvaranjaMax"] : string.Empty;
            int.TryParse(godinaMax, out int godinaOtvaranjaMax);

            Session["godinaMin"] = godinaOtvaranjaMin;
            Session["godinaMax"] = godinaOtvaranjaMax;

            return RedirectToAction("Pretraga");
        }
       
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Pretraga()
        {
            Korisnik korisnik = new Korisnik();
            if ((Korisnik)Session["PrijavljeniKorisnik"] != null)
            {
                korisnik = (Korisnik)Session["PrijavljeniKorisnik"];
            }
            if (korisnik.Uloga == Uloga.TRENER)
            {
                if (Load.KorisnikBlokiran(korisnik.Id))
                {
                    Session["AlertMessage"] = "Blokirani ste iz sistema!";
                    return RedirectToAction("Logout", "Authentication");
                }
            }
            ViewBag.korisnik = korisnik;

            string naziv = (string)Session["HomeNaziv"];
            string adresa = (string)Session["HomeAdresa"];

            int godinaOtvaranjaMin = (int)Session["godinaMin"];
            int godinaOtvaranjaMax = (int)Session["godinaMax"];

            Dictionary<int, FitnesCentar> fitnescentri = Load.FitnesCentriFromDataBase();
            List<FitnesCentar> fitnescentrilista = new List<FitnesCentar>();

            foreach (var item in fitnescentri)
            {
                if (godinaOtvaranjaMax != 0)
                {
                    if (item.Value.Naziv.ToLower().Contains(naziv.ToLower()) && item.Value.Adresa.ToLower().Contains(adresa.ToLower()) && item.Value.GodinaOtvaranja > godinaOtvaranjaMin && item.Value.GodinaOtvaranja < godinaOtvaranjaMax)
                    {
                        fitnescentrilista.Add(item.Value);
                    }
                }
                else
                {
                    if (item.Value.Naziv.ToLower().Contains(naziv.ToLower()) && item.Value.Adresa.ToLower().Contains(adresa.ToLower()) && item.Value.GodinaOtvaranja > godinaOtvaranjaMin)
                    {
                        fitnescentrilista.Add(item.Value);
                    }
                }               
            }

            List<FitnesCentar> sortirana = fitnescentrilista.OrderBy(l => l.Naziv).ToList();
            Session["naziv"] = 1;
            Session["adresa"] = 0;
            Session["godina"] = 0;

            Session["pretrazenicentri"] = sortirana.ToDictionary(x => x.Id);
            ViewBag.sortiranaLista = sortirana;
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}